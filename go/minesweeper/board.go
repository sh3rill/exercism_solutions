package minesweeper

import (
	"bytes"
	"errors"
)

// Board minesweeper board
type Board [][]byte

// String conert to string
func (b Board) String() string {
	return "\n" + string(bytes.Join(b, []byte{'\n'}))
}

// Count count mines
func (b Board) Count() error {
	for row := range b {
		for col, elem := range b[row] {
			if !b.isValid(row, col) {
				return errors.New("Invalid board")
			}
			// if cell is empty count mines
			if elem == ' ' {
				mines := 0
				// mines are counted one row up to one row down and one col left to one col right
				for i := row - 1; i <= row+1 && i < len(b); i++ {
					for j := col - 1; j <= col+1 && j < len(b[i]); j++ {
						if b[i][j] == '*' {
							mines++
						}
					}
				}
				// Only populate if more than one mine
				if mines > 0 {
					b[row][col] = byte(mines) + '0'
				}
			}
		}
	}
	return nil
}

// isValid check if cell item is valid
func (b Board) isValid(r, c int) bool {
	elem := b[r][c]
	lastrow := len(b) - 1
	lastcol := len(b[0]) - 1
	switch elem {
	case '+':
		return (c == 0 && r == 0) || (c == 0 && r == lastrow) || (c == lastcol && r == 0) || (c == lastcol && r == lastrow)
	case '|':
		return c == 0 || c == lastcol
	case '-':
		return r == 0 || r == lastrow
	case '*':
		return r > 0 && r < lastrow && c > 0 && c < lastcol
	case ' ':
		return r > 0 && r < lastrow && c > 0 && c < lastcol
	default:
		return elem > '1' && elem <= '8' && r > 0 && r < lastrow && c > 0 && c < lastcol
	}
}
