package school

import (
	"fmt"
)

const (
	// MinGrade grades start at MinGrade
	MinGrade = 1
	// MaxGrade maximum number of allowed grades
	MaxGrade = 12
	// CountGrade number of valid grades
	CountGrade = MaxGrade - MinGrade + 1
)

// Grade collection of names with one tag
type Grade struct {
	Number int
	Names  []string
}

// School collection of grades
type School struct {
	grades []Grade
}

// String convert grade to string
func (g Grade) String() string {
	if g.Number < MinGrade || g.Number > MaxGrade {
		return ""
	}
	if len(g.Names) == 0 {
		return fmt.Sprintf("Grade %d:", g.Number)
	}
	names := ""

	for _, name := range g.Names {
		names += " " + name
	}

	return fmt.Sprintf("Grade %d: %s.", g.Number, names)
}

// Add add a name in grade
func (g *Grade) Add(name string) {
	g.Names = append(g.Names, name)
	i := 0
	for i = len(g.Names) - 1; i > 0; i-- {
		if g.Names[i] < g.Names[i-1] {
			g.Names[i], name = g.Names[i-1], g.Names[i]
		} else {
			break
		}
	}
	g.Names[i] = name
}

// Clone clone a grade
func (g *Grade) Clone() Grade {
	s := make([]string, len(g.Names))
	copy(s, g.Names)
	return Grade{g.Number, s}
}

// New create new school
func New() *School {
	g := make([]Grade, CountGrade)
	// Initialize all grades
	for i := 0; i < CountGrade; i++ {
		g[i].Number = i + MinGrade
		g[i].Names = make([]string, 0, 10)
	}
	return &School{g}
}

// Add add a student in a grade
func (s *School) Add(name string, grade int) {
	if grade < MinGrade || grade > MaxGrade {
		return
	}
	// Add name in grade
	s.grades[grade-MinGrade].Add(name)
}

// Grade get names from a grade
func (s *School) Grade(grade int) []string {
	if grade < MinGrade || grade > MaxGrade {
		return nil
	}
	// Returned a cloned slice
	g := s.grades[grade-MinGrade].Clone()
	return g.Names
}

// Enrollment get all grades with atleast one student
func (s *School) Enrollment() []Grade {
	grades := make([]Grade, 0, CountGrade)
	for _, grade := range s.grades {
		if len(grade.Names) > 0 {
			grades = append(grades, grade.Clone())
		}
	}
	return grades
}
