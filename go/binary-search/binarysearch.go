package binarysearch

// SearchInts binary search in slice of ints
func SearchInts(arr []int, key int) int {
	start := 0
	end := len(arr)
	for start < end {
		mid := (start + end) / 2
		if key == arr[mid] {
			return mid
		} else if key > arr[mid] {
			start = mid + 1
		} else {
			end = mid
		}
	}
	return -1
}
