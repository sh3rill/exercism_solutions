package collatzconjecture

import (
	"errors"
	"math"
)

var cache = map[int]int{}

/* Recursive Solution when lots of varying requests comes
func conjecture(n int) int {
	if n <= 1 {
		return 0
	}
	if v, ok := cache[n]; ok {
		fmt.Println("Cache Hit", n, v)
		return v
	}
	m := 0
	if n&01 == 0 {
		m = n >> 1
	} else {
		m = 3*n + 1
	}
	step := conjecture(m) + 1
	cache[n] = step
	return step
}
*/

// CollatzConjecture run unitl number is converted to 1
func CollatzConjecture(n int) (int, error) {
	if n <= 0 {
		return -1, errors.New("Only positive number allowed")
	}
	step := 0
	for n != 1 {
		if v, ok := cache[n]; ok {
			return step + v, nil
		}
		step++
		if n&01 == 0 {
			n >>= 1
		} else {
			n = 3*n + 1
		}
		if step == math.MaxInt32 {
			return -1, errors.New("Number overflow")
		}
	}
	cache[n] = step
	return step, nil
}
