package twelve

var days = [12]string{
	"first",
	"second",
	"third",
	"fourth",
	"fifth",
	"sixth",
	"seventh",
	"eighth",
	"ninth",
	"tenth",
	"eleventh",
	"twelfth",
}
var gifts = [12]string{
	"a Partridge in a Pear Tree.",
	"two Turtle Doves",
	"three French Hens",
	"four Calling Birds",
	"five Gold Rings",
	"six Geese-a-Laying",
	"seven Swans-a-Swimming",
	"eight Maids-a-Milking",
	"nine Ladies Dancing",
	"ten Lords-a-Leaping",
	"eleven Pipers Piping",
	"twelve Drummers Drumming",
}

func getGifts(day int) string {
	s := ""
	if day == 0 {
		return gifts[0]
	}
	for i := day; i > 0; i-- {
		if s == "" {
			s = gifts[i]
		} else {
			s += ", " + gifts[i]
		}
	}
	return s + ", and " + gifts[0]
}

func Song() string {
	song := ""
	for i := 0; i <= 11; i++ {
		song += "On the " + days[i] + " day of Christmas my true love gave to me, " + getGifts(i) + "\n"
	}
	return song
}

func Verse(day int) string {
	day--
	return "On the " + days[day] + " day of Christmas my true love gave to me, " + getGifts(day)
}
