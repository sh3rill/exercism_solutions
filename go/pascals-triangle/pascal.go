package pascal

// Generate row using prev row
func pascals(row int, prev []int) []int {
	r := make([]int, row)
	r[0] = 1
	r[row-1] = 1
	for i := 1; i < row-1; i++ {
		r[i] = prev[i-1] + prev[i]
	}
	return r
}

// Triangle get the pascals triangle
func Triangle(n int) [][]int {
	p := make([][]int, n)
	p[0] = pascals(1, nil)
	for i := 1; i < n; i++ {
		p[i] = pascals(i+1, p[i-1])
	}
	return p
}
