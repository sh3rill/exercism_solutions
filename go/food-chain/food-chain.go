package foodchain

var foods = []string{
	"fly",
	"spider",
	"bird",
	"cat",
	"dog",
	"goat",
	"cow",
	"horse",
}

var desc = []string{
	"\nI don't know why she swallowed the fly. Perhaps she'll die.",
	"\nIt wriggled and jiggled and tickled inside her.",
	"\nHow absurd to swallow a bird!",
	"\nImagine that, to swallow a cat!",
	"\nWhat a hog, to swallow a dog!",
	"\nJust opened her throat and swallowed a goat!",
	"\nI don't know how she swallowed a cow!",
	"\nShe's dead, of course!",
}

func prepare(n int) string {
	s := ""
	if n >= 0 {
		if n > 0 {
			s += "\nShe swallowed the " + foods[n] + " to catch the " + foods[n-1]
			if n == 2 {
				s += " that wriggled and jiggled and tickled inside her"
			}
			s += "." + prepare(n-1)
		}
	}
	return s
}

func Verse(n int) string {
	n--
	s := ""
	if n < len(foods)-1 && n >= 0 {
		s = "I know an old lady who swallowed a " + foods[n] + "."
		s += desc[n] + prepare(n)
		if n > 0 {
			s += desc[0]
		}
	} else if n == len(foods)-1 {
		s = "I know an old lady who swallowed a " + foods[n] + "."
		s += desc[n]
	}
	return s
}

func Song() string {
	return Verses(1, len(foods))
}

func Verses(n, m int) string {
	s := ""
	for i := n; i < m; i++ {
		s += Verse(i) + "\n\n"
	}
	return s + Verse(m)
}
