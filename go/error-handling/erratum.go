package erratum

// Use open a resource and handle error
func Use(o ResourceOpener, input string) (err error) {
	var r Resource
	for r == nil {
		r, err = o()
		if err != nil {
			if _, ok := err.(TransientError); !ok {
				return err
			}
			r = nil
		}
	}
	defer func() {
		if e := recover(); e != nil {
			if _err, ok := e.(FrobError); ok {
				r.Defrob(_err.defrobTag)
				err = _err
			} else {
				err = e.(error)
			}
		}
		r.Close()
	}()
	r.Frob(input)
	return err
}
