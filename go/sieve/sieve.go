package sieve

// Sieve get primes using sieve
func Sieve(limit int) []int {
	var primes []int
	if limit < 1 {
		return nil
	} else if limit == 1 {
		return []int{}
	} else if limit == 2 {
		return []int{2}
	}
	count := limit + 1

	numbers := make([]int, 0, limit-3)
	n := 3
	for i := 0; i < cap(numbers) && n <= limit; i++ {
		numbers = append(numbers, n)
		n += 2
	}
	for i := 0; i < len(numbers); i++ {
		if numbers[i] != 0 {
			for j := i + 1; j < len(numbers); j++ {
				if numbers[j] != 0 && numbers[j]%numbers[i] == 0 {
					numbers[j] = 0
					count--
				}
			}
		}
	}
	primes = make([]int, 1, count)
	primes[0] = 2
	for i := 0; i < len(numbers); i++ {
		if numbers[i] != 0 {
			primes = append(primes, numbers[i])
		}
	}
	return primes
}
