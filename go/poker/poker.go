package poker

import (
	"errors"
	"fmt"
	"sort"
	"strings"
	"unicode/utf8"
)

// HandType Type of Hand
type HandType int

const (
	None HandType = iota
	StraightFlush
	FourOfKind
	FullHouse
	Flush
	Straight
	ThreeOfKind
	TwoPair
	OnePair
	HighCard
)

// MaxCardsPerType maximum number of cards in a type
const MaxCardsPerType = 14*14*14*14*14*14 + 14*14*14*14*14 + 14*14*14*14 + 14*14*14 + 14*14

// Card Card
type Card struct {
	rank int
	kind rune
}

var validCardsLight = [4]rune{'♤', '♡', '♢', '♧'}
var validCardsDark = [4]rune{'♠', '♥', '♦', '♣'}

// Hand Sort hand
type Hand struct {
	cards       []*Card
	handType    HandType
	counterKind map[rune]int
	counterRank map[int]int
	rep         string
	rank        int
}

// HandTypeStr conert hand type to string representation
var HandTypeStr = []string{
	"None",
	"StraightFlush",
	"FourOfKind",
	"FullHouse",
	"Flush",
	"Straight",
	"ThreeOfKind",
	"TwoPair",
	"OnePair",
	"HighCard",
}

func (t HandType) String() string {
	return HandTypeStr[t]
}

func (h *Hand) Len() int {
	return len(h.cards)
}

func (h *Hand) Swap(i, j int) {
	h.cards[i], h.cards[j] = h.cards[j], h.cards[i]
}

func (h *Hand) Less(i, j int) bool {
	return h.cards[i].rank < h.cards[j].rank
}

// Add a card into a hand
func (h *Hand) Add(c *Card) bool {
	if len(h.cards) == 5 {
		return false
	}
	h.cards = append(h.cards, c)
	return true
}

// Update Update hand
func (h *Hand) Update() {
	h.counterRank = make(map[int]int)
	h.counterKind = make(map[rune]int)
	h.Sort()
	// Counts counter for each card
	for _, c := range h.cards {
		h.counterKind[c.kind]++
		h.counterRank[c.rank]++
	}
}

// Flush check is this a flush
func (h *Hand) Flush() bool {
	return len(h.counterKind) == 1
}

// Sort sort cards for this hand
func (h *Hand) Sort() {
	sort.Sort(h)
}

// Straight Check if this is a straight
func (h *Hand) Straight() bool {
	var last *Card
	// Check if its a flush
	for i, c := range h.cards {
		if i == 0 {
			last = c
		} else if last.rank+1 != c.rank {
			// When 2-5 is used A becomes 1 instead of 14
			if last.rank != 5 || c.rank != 14 {
				return false
			}
			// A is used as 1
			c.rank = 1
			// Update counters
			h.Update()
		}
		last = c
	}
	return true
}

// Process process cards in this hand
func getType(h *Hand) HandType {
	// Update Cards
	h.Update()

	// Check rank counters
	switch len(h.counterRank) {
	case 1:
		// Not possible without wild card
		return None
	case 2:
		// 4,1 or 3,2
		for _, count := range h.counterRank {
			if count == 4 || count == 1 {
				return FourOfKind
			} else if count == 2 || count == 3 {
				return FullHouse
			}
		}
	case 3:
		// 3,1,1 or 2,2,1
		for _, count := range h.counterRank {
			if count == 3 {
				return ThreeOfKind
			} else if count == 2 {
				return TwoPair
			}
		}
	case 4:
		// 4, 1
		return OnePair

	case 5:
		straight := h.Straight()
		// Check if this is a flush
		if h.Flush() {
			if straight {
				return StraightFlush
			}
			return Flush
		} else if straight {
			return Straight
		}

		return HighCard

	}
	return None
}

// Keys get keys from map
func Keys(m map[int]int) []int {
	keys := make([]int, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}
	sort.Slice(keys, func(i, j int) bool {
		if m[keys[i]] != m[keys[j]] {
			return m[keys[i]] < m[keys[j]]
		}
		return keys[i] < keys[j]
	})
	return keys
}

// Polinomial calculate Polinomial value for given x and coefficenet
func Polinomial(coef []int, x int) int {
	val := 0
	base := x
	for _, k := range coef {
		val += base * k
		base = base * x
	}
	return val
}

// Rank get a numerical rank for this hand
func (h *Hand) Rank() int {
	if h.rank == -1 {
		h.handType = getType(h)
		val := Polinomial(Keys(h.counterRank), 14)
		delta := int(HighCard - h.handType)
		h.rank = delta*MaxCardsPerType + val
	}
	return h.rank
}

// Value get value for card
func (c *Card) Value() int {
	return c.rank
}

// Kind get kind for this card
func (c *Card) Kind() rune {
	return c.kind
}

func (c *Card) String() string {
	if c.rank == 11 {
		return fmt.Sprintf("J%c", c.kind)
	} else if c.rank == 12 {
		return fmt.Sprintf("Q%c", c.kind)
	} else if c.rank == 13 {
		return fmt.Sprintf("K%c", c.kind)
	} else if c.rank == 14 || c.rank == 1 {
		return fmt.Sprintf("A%c", c.kind)
	}
	return fmt.Sprintf("%d%c", c.rank, c.kind)
}

func (h *Hand) String() string {
	s := ""
	for _, c := range h.cards {
		if s == "" {
			s = c.String()
		} else {
			s += " " + c.String()
		}
	}
	s += "=>" + h.handType.String()
	return s
}

// NewCard get new card
func NewCard() *Card {
	return &Card{rank: -1}
}

// NewHand get a new hand
func NewHand(rep string) *Hand {
	h := Hand{cards: make([]*Card, 0, 5), rep: rep, rank: -1}
	return &h
}

// Parse Card
func (c *Card) Parse(str string, validCards [4]rune) error {
	kind, l := utf8.DecodeLastRuneInString(str)
	rep := str[:len(str)-l]
	rank := 0
	// Check if this is a valid kind
	if kind != validCards[0] && kind != validCards[1] &&
		kind != validCards[2] && kind != validCards[3] {
		return errors.New("Invalid kind")
	}
	// Card has rank along with kind
	if l == len(str) {
		return errors.New("Invalid length for card")
	}
	// Get Rank of kard
	ch, l := utf8.DecodeLastRuneInString(rep)
	// Only 10 has 3 char others only has 2
	if l == len(rep) {
		// Numerical Representation
		if ch >= '2' && ch <= '9' {
			rank = int(ch - '0')
		} else if ch == 'J' {
			rank = 11
		} else if ch == 'Q' {
			rank = 12
		} else if ch == 'K' {
			rank = 13
		} else if ch == 'A' {
			rank = 14
		} else {
			// Invalid Card
			return errors.New("Invalid card rank")
		}
	} else if ch == '0' {
		// Only 10
		rep = rep[:len(rep)-l]
		ch, l = utf8.DecodeLastRuneInString(rep)
		if ch != '1' || l != len(rep) {
			return errors.New("Invalid card rank2")
		}
		rank = 10
	} else {
		// Invalid Rank
		return errors.New("Invalid length for card")
	}
	// Set Kind and Rank
	c.kind = kind
	c.rank = rank
	return nil
}

// ParseHand parse single hand
func ParseHand(str string) (*Hand, error) {
	parts := strings.Fields(str)
	h := NewHand(str)
	for _, part := range parts {
		// Get a new Card
		c := NewCard()
		// Parse this card
		if err := c.Parse(part, validCardsLight); err != nil {
			return nil, err
		}
		// If Add fails we have more cards
		if !h.Add(c) {
			return nil, errors.New("More than 5 cards")
		}
	}
	// Check if we have all cards
	if len(h.cards) != cap(h.cards) {
		return nil, errors.New("Less than 5 cards")
	}
	return h, nil
}

// BestHand get best hand out of all hands, for tie more than one can be returned
func BestHand(rep []string) ([]string, error) {
	var winner []string
	var winnerRank int
	// Get Hand with max Rank
	for _, str := range rep {
		hand, err := ParseHand(str)
		if err != nil {
			return nil, err
		}

		r := hand.Rank()
		if r < winnerRank {
			continue
		} else if r > winnerRank {
			winner = nil
		}

		winner = append(winner, hand.rep)
		winnerRank = r
	}
	return winner, nil
}
