package atbash

// Atbash atbash encryption
func Atbash(plain string) string {
	cypher := make([]rune, len(plain)+len(plain)/5+1)
	j := 0
	for _, ch := range plain {
		if ch >= 'A' && ch <= 'Z' {
			delta := ch - 'A'
			cypher[j] = 'z' - delta
			j++
		} else if ch >= 'a' && ch <= 'z' {
			delta := ch - 'a'
			cypher[j] = 'z' - delta
			j++
		} else if ch >= '0' && ch <= '9' {
			cypher[j] = ch
			j++
		}
		if (j+1)%6 == 0 {
			cypher[j] = ' '
			j++
		}
	}
	if cypher[j-1] == ' ' {
		j--
	}
	s := string(cypher)
	// strip last extra char
	return s[0:j]
}
