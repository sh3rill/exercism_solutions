package space

type Planet string

// Age get space age
func Age(seconds float64, planet Planet) float64 {
	age := seconds / float64(31557600)
	switch planet {
	case "Earth":
		return age
	case "Mercury":
		return age / 0.2408467
	case "Venus":
		return age / 0.61519726
	case "Mars":
		return age / 1.8808158
	case "Jupiter":
		return age / 11.862615
	case "Saturn":
		return age / 29.447498
	case "Uranus":
		return age / 84.016846
	case "Neptune":
		return age / 164.79132
	}
	return age
}
