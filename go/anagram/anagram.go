package anagram

import (
	"sort"
	"strings"
)

// Detect detect anagrams of a string among candidates
func Detect(subject string, candidates []string) []string {
	repeats := map[string]struct{}{}
	sub := strings.ToLower(subject)
	repeats[sub] = struct{}{}
	str := []rune(sub)
	sort.Slice(str, func(i, j int) bool { return str[i] < str[j] })
	sortedSubject := string(str)
	result := make([]string, 0, len(candidates))
	for _, candidate := range candidates {
		c := strings.ToLower(candidate)
		a := []rune(c)
		sort.Slice(a, func(i, j int) bool { return a[i] < a[j] })
		b := string(a)
		if b == sortedSubject {
			if _, ok := repeats[c]; !ok {
				result = append(result, candidate)
				repeats[c] = struct{}{}
			}
		}
	}
	return result
}
