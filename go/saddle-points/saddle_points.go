package matrix

// Pair pair ow two numbers
type Pair [2]int

// Max get max of an integer slice
func Max(arr []int) (int, int) {
	max := arr[0]
	maxi := 0
	for i, n := range arr {
		if max < n {
			maxi = i
			max = n
		}
	}
	return maxi, max
}

// Saddle get saddle points
func (m *Matrix) Saddle() []Pair {
	p := make([]Pair, 0)
OUTER:
	for i := 0; i < m.row; i++ {
		j, max := Max(m.elem[i])
		// Check this is min in its row
		for k := 0; k < m.row; k++ {
			if k != i && m.elem[k][j] < max {
				continue OUTER
			}
		}
		p = append(p, Pair{i, j})
	}
	return p
}
