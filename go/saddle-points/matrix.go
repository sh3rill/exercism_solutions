package matrix

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

// Row Matrix row
type Row []int

// Col Matrix col
type Col []int

// Matrix 2 d matrix
type Matrix struct {
	row, col int
	elem     [][]int
}

// String convert matrix to string
func (m *Matrix) String() string {
	s := "\n[\n"
	for i := 0; i < m.row; i++ {
		for j := 0; j < m.col; j++ {
			s += fmt.Sprintf("%4s", strconv.Itoa(m.elem[i][j]))
		}
		s += "\n"
	}
	s += "]\n"
	return s
}

// New get a new matrix
func New(s string) (*Matrix, error) {
	lines := strings.Split(s, "\n")
	col := 0
	row := len(lines)
	m := make([][]int, row)
	for i := 0; i < row; i++ {
		parts := strings.Split(strings.TrimSpace(lines[i]), " ")
		if i == 0 {
			col = len(parts)
		} else if col != len(parts) {
			return nil, errors.New("Matrix col lenght mismatch")
		}
		m[i] = make([]int, col)
		for j := 0; j < len(parts); j++ {
			n, err := strconv.Atoi(parts[j])
			if err != nil {
				return nil, err
			}
			m[i][j] = n
		}
	}

	return &Matrix{row: row, col: col, elem: m}, nil
}

// Rows get matrix rows
func (m *Matrix) Rows() [][]int {
	return m.elem
}

// Cols get matrix cols
func (m *Matrix) Cols() [][]int {
	var cols = make([][]int, m.col)
	for i := 0; i < m.col; i++ {
		cols[i] = make([]int, m.row)
		for j := 0; j < m.row; j++ {
			cols[i][j] = m.elem[j][i]
		}
	}
	return cols
}

// Set set element of matrix at row and col
func (m *Matrix) Set(r, c, val int) bool {
	if r >= 0 && r < m.row && c >= 0 && c < m.col {
		m.elem[r][c] = val
		return true
	}
	return false
}
