package robot

import (
	"fmt"
	"time"
)

// Action3 action to be sent to room
type Action3 struct {
	name string
	val  Action
}

func (a Action3) String() string {
	return fmt.Sprintf("Action<%s : %v>", a.name, a.val)
}

func (r Step3Robot) String() string {
	return fmt.Sprintf("Robot<Name : %s, Pos : %v>", r.Name, r.Pos)
}

// Equals check if two robots are same
func (r *Step3Robot) Equals(r1 *Step3Robot) bool {
	return r.Name == r1.Name
}

// Collided check if two robots have collided
func (r *Step3Robot) Collided(r1 *Step3Robot) bool {
	return r.Pos.Equals(r1.Pos)
}

// Arena robots arena
type Arena struct {
	robots  []Step3Robot
	mapping map[string]*Step3Robot
	report  chan<- []Step3Robot
	logger  chan<- string
	alive   int
	rect    Rect
	active  bool
}

// NewArena create new arena
func NewArena(count int, extent Rect, report chan<- []Step3Robot, log chan<- string) *Arena {
	return &Arena{
		active:  true,
		rect:    extent,
		robots:  make([]Step3Robot, 0, count),
		report:  report,
		logger:  log,
		mapping: map[string]*Step3Robot{}}

}

// CheckRobotPos check if they share same poistion
func (a *Arena) CheckRobotPos(r *Step3Robot) bool {
	for i := 0; i < len(a.robots); i++ {
		r1 := &a.robots[i]
		if r != r1 && r.Collided(r1) {
			//fmt.Println("Collision")
			return false
		}
	}
	return true
}

// AddRobot add new robot
func (a *Arena) AddRobot(robot Step3Robot) bool {
	// Check for duplicates
	if robot.Name == "" {
		a.Log("Robot Name is Null")
		return false
	}
	if _, ok := a.mapping[robot.Name]; ok {
		a.Log("Duplicate Name :" + robot.Name)
		return false
	}
	if !a.rect.Valid(robot.Pos) {
		a.Log("Robot Outside of room " + robot.Name)
		return false
	}
	// Number of Robots already added
	l := len(a.robots)
	if l > 0 {
		if !a.CheckRobotPos(&robot) {
			a.Log("Robot share position")
		}
	}
	a.robots = append(a.robots, robot)
	a.mapping[robot.Name] = &a.robots[l]
	a.alive++
	//fmt.Println("Add Robot", robot)
	return true
}

// Action perform action on robot
func (a *Arena) Action(act Action3) bool {
	// Run action
	if r, ok := a.mapping[act.name]; ok {
		switch act.val {
		case DirLeft:
			r.Left()
		case DirRight:
			r.Right()
		case Move:
			oldPos := r.Pos
			r.Advance()
			if !a.rect.Valid(r.Pos) {
				a.Log("Wall Hit")
				r.Pos = oldPos
			} else if !a.CheckRobotPos(r) {
				r.Pos = oldPos
				a.Log("Robot Collision")
			}
		case Shutdown:
			a.alive--
		}
		return true
	}

	a.Log("Unknown Robot :" + act.name)
	return false
}

// Log log a message
func (a *Arena) Log(message string) {
	a.logger <- message
}

// Close send report for all robots
func (a *Arena) Close() {
	a.report <- a.robots
	a.active = false
}

// Global arena
var arena *Arena

// Room3 create robot
func Room3(extent Rect, robots []Step3Robot, action chan Action3, report chan []Step3Robot, log chan string) {

	// Check if there is already an active arena
	if arena != nil && arena.active {
		fmt.Println("Previous arena still active")
	}

	// Create New arena
	arena = NewArena(len(robots), extent, report, log)
	for _, robot := range robots {
		if !arena.AddRobot(robot) {
			arena.Close()
			return
		}
	}
	// Wait for signal on action channel
	for act := range action {
		if !arena.Action(act) || arena.alive == 0 {
			break
		}
	}
	// Close arena
	arena.Close()
}

// StartRobot3 start robot
func StartRobot3(name, script string, action chan<- Action3, log chan<- string) {
	// Check name is not empty
	if name == "" {
		return
	}

	// Wait for arena creation
	for arena == nil {
		fmt.Println("Arena not created. Wait...")
		time.Sleep(time.Millisecond * 500)
	}
	// Execute the scipt
EXECUTION:
	for _, ch := range script {
		switch ch {
		case 'R':
			action <- Action3{name, DirRight}
		case 'L':
			action <- Action3{name, DirLeft}
		case 'A':
			action <- Action3{name, Move}
		default:
			log <- "Undefined command : " + string(ch) + " : " + name
			break EXECUTION
		}
	}
	// Send Shutdown signal
	action <- Action3{name, Shutdown}
}
