package robot

import (
	"fmt"
)

// Action action
type Action int

const (
	// Move advance to new cell
	Move Action = iota
	// DirLeft set direction to left
	DirLeft
	// DirRight set direction to right
	DirRight
	// Shutdown robot is shutdown
	Shutdown
	// Undefined command
	Undefined
)

var actionMap = map[Action]string{
	Move:     "Move",
	DirLeft:  "Left",
	DirRight: "Right",
	Shutdown: "Shutdown",
}

func (a Action) String() string {
	return actionMap[a]
}

func (p Pos) String() string {
	return fmt.Sprintf("{%d,%d}", p.Easting, p.Northing)
}

func (p Pos) Equals(q Pos) bool {
	return p.Easting == q.Easting && p.Northing == q.Northing
}

// Valid check if position is bounded by rectangle
func (r Rect) Valid(p Pos) bool {
	return p.Easting >= r.Min.Easting && p.Easting <= r.Max.Easting &&
		p.Northing >= r.Min.Northing && p.Northing <= r.Max.Northing
}

// StartRobot start robot
func StartRobot(cmd chan Command, act chan Action) {
	for c := range cmd {
		switch c {
		case 'R':
			act <- DirRight
		case 'L':
			act <- DirLeft
		case 'A':
			act <- Move
		}
	}
	act <- Shutdown
}

// Room create room
func Room(extent Rect, robot Step2Robot, act chan Action, rep chan Step2Robot) {
	r := &robot
	for a := range act {
		switch a {
		case DirLeft:
			r.Left()
		case DirRight:
			r.Right()
		case Move:
			oldPos := r.Pos
			r.Advance()
			if !extent.Valid(r.Pos) {
				r.Pos = oldPos
			}
		case Shutdown:
			rep <- robot
		}
	}
}

// Left change dir to left
func (r *Step2Robot) Left() {
	r.Dir = r.Dir.Left()
}

// Right move right
func (r *Step2Robot) Right() {
	r.Dir = r.Dir.Right()
}

// Advance robot
func (r *Step2Robot) Advance() {
	switch r.Dir {
	case N:
		r.Northing++
	case E:
		r.Easting++
	case S:
		r.Northing--
	case W:
		r.Easting--
	}
	//fmt.Println("New Pos", r.Pos)
}
