package robot

const (
	// N North
	N Dir = iota
	// E East
	E
	// S South
	S
	// W West
	W
)

// DirStr a map for dir to strings name
var DirStr = map[Dir]string{
	N: "North",
	E: "East",
	S: "South",
	W: "West",
}

func (d Dir) String() string {
	return DirStr[d]
}

// Left change dir to left
func (d Dir) Left() Dir {
	return (4 + d - 1) % 4
}

// Right change dir to right
func (d Dir) Right() Dir {
	return (4 + d + 1) % 4
}
