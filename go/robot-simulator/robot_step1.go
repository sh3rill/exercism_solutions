package robot

// Left change dir to left
func Left() {
	Step1Robot.Dir = Step1Robot.Dir.Left()
}

// Right move right
func Right() {
	Step1Robot.Dir = Step1Robot.Dir.Right()
}

// Advance robot
func Advance() {
	switch Step1Robot.Dir {
	case N:
		Step1Robot.Y++
	case E:
		Step1Robot.X++
	case S:
		Step1Robot.Y--
	case W:
		Step1Robot.X--
	}
}
