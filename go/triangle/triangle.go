// This is a "stub" file.  It's a little start on your solution.
// It's not a complete solution though; you have to write some code.

// Package twofer should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package triangle

import (
	"math"
)

// Kind KindFromSides() returns this type. Pick a suitable data type.
type Kind int

const (
	// Pick values for the following identifiers used by the test program.
	NaT Kind = iota // not a triangle
	Equ             // equilateral
	Iso             // isosceles
	Sca             // scalene
)

const EPSILON float64 = 0.00000001

func floatEquals(a, b float64) bool {
	return math.Abs(a-b) < EPSILON
}

// ShareWith should have a comment documenting it.
func KindFromSides(a, b, c float64) Kind {
	if a <= 0 || b <= 0 || c <= 0 ||
		math.IsNaN(a) || math.IsNaN(b) || math.IsNaN(c) ||
		math.IsInf(a, 0) || math.IsInf(b, 0) || math.IsInf(c, 0) ||
		a+b < c || b+c < a || a+c < b {
		return NaT
	} else if floatEquals(a, b) {
		if floatEquals(a, c) {
			return Equ
		}
		return Iso
	} else if floatEquals(a, c) || floatEquals(b, c) {
		return Iso
	}
	return Sca
}
