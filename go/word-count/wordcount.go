package wordcount

import (
	"strings"
	"unicode"
)

type Frequency map[string]int

func WordCount(phrase string) Frequency {
	f := Frequency{}
	words := strings.FieldsFunc(phrase, func(r rune) bool {
		return !(unicode.IsLetter(r) ||
			unicode.IsDigit(r) ||
			r == '\'' || r == '"')
	})
	for _, word := range words {
		l := len(word)
		if (word[0] == '\'' && word[l-1] == '\'') ||
			(word[0] == '"' && word[l-1] == '"') {
			word = word[1:(l - 1)]
		}
		w := strings.ToLower(word)
		f[w]++
	}
	return f
}
