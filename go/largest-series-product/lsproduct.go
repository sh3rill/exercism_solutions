package lsproduct

import (
	"errors"
)

func product(s string) int {
	p := 1
	for _, val := range s {
		if val >= '0' && val <= '9' {
			p = p * int((val - '0'))
		} else {
			return -1
		}
	}
	return p
}

func LargestSeriesProduct(digits string, span int) (int, error) {
	if span >= 0 && span <= len(digits) {
		p := 0
		max := 0
		for i := 0; i <= len(digits)-span; i++ {
			p = product(digits[i : i+span])
			if p < 0 {
				return -1, errors.New("Non digit char")
			}
			if p > max {
				max = p
			}
		}
		return max, nil
	}
	return 0, errors.New("Span can not be greater rthan length of digits")
}
