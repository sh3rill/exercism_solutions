package ocr

import (
	"strings"
)

var digits = []string{
	`
 _ 
| |
|_|
   `, `
   
  |
  |
   `, `
 _ 
 _|
|_ 
   `, `
 _ 
 _|
 _|
   `, `
   
|_|
  |
   `, `
 _ 
|_ 
 _|
   `, `
 _ 
|_ 
|_|
   `, `
 _ 
  |
  |
   `, `
 _ 
|_|
|_|
   `, `
 _ 
|_|
 _|
   `,
}

func recognizeDigit(lines ...string) byte {
	input := strings.Join(lines, "\n")
	for i := 0; i < len(digits); i++ {
		if digits[i] == input {
			return byte(i + '0')
		}
	}
	return '?'
}

func recognizeNumber(line1, line2, line3, line4 string) string {
	count := len(line1) / 3
	n := make([]byte, count)
	for i, j := 0, 0; i <= len(line1)-3; i, j = i+3, j+1 {
		n[j] = recognizeDigit("", line1[i:i+3], line2[i:i+3], line3[i:i+3], line4[i:i+3])
	}
	return string(n)
}

func Recognize(input string) []string {
	lines := strings.Split(input, "\n")
	count := len(lines)
	count--
	n := make([]string, count/4)
	for i, j := 1, 0; i <= len(lines)-4; i, j = i+4, j+1 {
		n[j] = recognizeNumber(lines[i], lines[i+1], lines[i+2], lines[i+3])
	}
	return n
}
