package accumulate

func Accumulate(collection []string,
	converter func(string) string) []string {
	list := make([]string, len(collection))
	for i := range collection {
		list[i] = converter(collection[i])
	}
	return list
}
