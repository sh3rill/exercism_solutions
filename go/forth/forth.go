package forth

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"unicode"
)

// node stack node
type node struct {
	data int
	next *node
}

// Stack stack for interpreter
type Stack struct {
	head  *node
	count int
}

// Interpreter Forth interpreter
type Interpreter struct {
	stack     *Stack
	functions map[string][]string
}

// Token token type
type Token int

const (
	// None invalid token
	None Token = iota
	// FunctionStart start of function definition
	FunctionStart
	// FunctionEnd end of function definition
	FunctionEnd
	// Function function in interpreter
	Function
	// Number number
	Number
	// String simple string
	String
	// Plus Addition
	Plus
	// Minus subtraction
	Minus
	// Multiply multiplication
	Multiply
	// Divide division
	Divide
)

// TokenStr token string representation
var TokenStr = []string{
	"None",
	"FunctionStart",
	"FunctionEnd",
	"Function",
	"Number",
	"String",
	"Plus",
	"Minus",
	"Multiply",
}

func (t Token) String() string {
	return TokenStr[t]
}

func (n *node) String() string {
	return fmt.Sprintf("{%d, ->%v}", n.data, n.next)
}

func (s *Stack) String() string {
	return fmt.Sprintf("Stack:[%v]", s.head)
}

// New get new interpreter
func New() *Interpreter {
	s := NewStack()
	f := make(map[string][]string)
	return &Interpreter{functions: f, stack: s}
}

// NewStack get new stack
func NewStack() *Stack {
	s := Stack{}
	return &s
}

// Push push number on stack
func (s *Stack) Push(data int) {
	n := &node{data: data, next: s.head}
	s.head = n
	s.count++
}

// Pop pop an element from stack
func (s *Stack) Pop() (int, error) {
	if s.head == nil {
		return -1, errors.New("")
	}
	n := s.head
	s.head = n.next
	n.next = nil
	s.count--
	return n.data, nil
}

// Peek take a look at last element
func (s *Stack) Peek() (int, error) {
	if s.head == nil {
		return -1, errors.New("")
	}
	n := s.head
	return n.data, nil
}

// Size size of stack
func (s *Stack) Size() int {
	return s.count
}

// parseToken parse string to token
func (interpreter *Interpreter) parseToken(str string) Token {
	// Check of we have created a function for this
	if _, ok := interpreter.functions[str]; ok {
		return Function
	}
	switch str {
	case ":":
		return FunctionStart
	case ";":
		return FunctionEnd
	case "+":
		return Plus
	case "-":
		return Minus
	case "*":
		return Multiply
	case "/":
		return Divide
	}

	digitFound := false
	alphaFound := false

	// Check if it is alphabets, numerical or aphanumerical
	for _, ch := range str {
		if unicode.IsDigit(ch) {
			digitFound = true
		} else if unicode.IsLetter(ch) {
			alphaFound = true
		}
	}
	// aphanumerical
	if alphaFound {
		return String
	}
	// numerical
	if digitFound {
		return Number
	}
	return None
}

// newFunction new function definition
func (interpreter *Interpreter) newFunction(parts []string) (int, error) {
	if parts[0] != ":" {
		return -1, errors.New("Format Error, Expecting : found " + parts[0])
	}
	tname := strings.ToLower(parts[1])
	token := interpreter.parseToken(tname)
	if token == FunctionEnd || token == Number {
		return -1, errors.New("Format Error, Expecting function name found " + tname)
	}
	for j := 2; j < len(parts); j++ {
		// parseToken if this is a function end
		if interpreter.parseToken(parts[j]) == FunctionEnd {
			interpreter.functions[strings.ToLower(tname)] = parts[2:j]
			return j, nil
		}
	}
	return -1, errors.New("Function not ended")
}

// Operation run a operation on stack with arguments and update stack with the result
func (s *Stack) Operation(count int, f func([]int) ([]int, error)) error {
	args := make([]int, count)
	// Get all elements from stack
	for i := 0; i < count; i++ {
		m, err := s.Pop()
		if err != nil {
			return err
		}
		args[i] = m
	}

	// Run function with arguments
	arr, err := f(args)
	if err != nil {
		return err
	}
	// Save all elements on stack
	for _, v := range arr {
		s.Push(v)
	}
	return nil
}

// Script Run a script on current stack
func (interpreter *Interpreter) Script(inputs []string) error {
	for i := 0; i < len(inputs); i++ {
		part := strings.ToLower(inputs[i])
		stack := interpreter.stack
		switch interpreter.parseToken(part) {
		case Function:
			script := interpreter.functions[part]
			if err := interpreter.Script(script); err != nil {
				return err
			}
		case Plus:
			if err := stack.Operation(2, func(args []int) ([]int, error) {
				return []int{args[0] + args[1]}, nil
			}); err != nil {
				return err
			}
		case Minus:
			if err := stack.Operation(2, func(args []int) ([]int, error) {
				return []int{args[1] - args[0]}, nil
			}); err != nil {
				return err
			}
		case Multiply:
			if err := stack.Operation(2, func(args []int) ([]int, error) {
				return []int{args[0] * args[1]}, nil
			}); err != nil {
				return err
			}
		case Divide:
			if err := stack.Operation(2, func(args []int) ([]int, error) {
				if args[0] == 0 {
					return nil, errors.New("Divide by zero")
				}
				return []int{args[1] / args[0]}, nil
			}); err != nil {
				return err
			}
		case FunctionStart:
			j, err := interpreter.newFunction(inputs[i:])
			if err != nil {
				return err
			}
			i = j
		case Number:
			n, err := strconv.Atoi(part)
			if err != nil {
				return err
			}
			stack.Push(n)
		case String:
			if script, ok := interpreter.functions[part]; ok {
				err := interpreter.Script(script)
				if err != nil {
					return err
				}
			} else if part == "dup" {
				m, err := stack.Peek()
				if err != nil {
					return err
				}
				stack.Push(m)
			} else if part == "drop" {
				if err := stack.Operation(1, func(args []int) ([]int, error) {
					return []int{}, nil
				}); err != nil {
					return err
				}
			} else if part == "swap" {
				if err := stack.Operation(2, func(args []int) ([]int, error) {
					return []int{args[0], args[1]}, nil
				}); err != nil {
					return err
				}
			} else if part == "over" {
				if err := stack.Operation(2, func(args []int) ([]int, error) {
					return []int{args[1], args[0], args[1]}, nil
				}); err != nil {
					return err
				}
			} else {
				return errors.New("Unkonwn Command")
			}
		default:
			return errors.New("Function End With out start")
		}
	}
	return nil
}

// Forth run forth interpreter on arguments
func Forth(inputs []string) ([]int, error) {
	interpreter := New()
	for _, input := range inputs {
		parts := strings.Fields(input)
		err := interpreter.Script(parts)
		if err != nil {
			return nil, err
		}
	}
	arr := make([]int, interpreter.stack.Size())
	for i := interpreter.stack.Size() - 1; i >= 0; i-- {
		arr[i], _ = interpreter.stack.Pop()
	}
	return arr, nil
}
