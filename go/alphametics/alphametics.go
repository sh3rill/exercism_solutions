package alphametics

import (
	"errors"
	"unicode"
)

const (
	// NonDigit Invalid Digit
	NonDigit byte = 10
	// NonLetter Invalid letter
	NonLetter = 26
	// EmptyLetter letter that does not have any value
	EmptyLetter = 11
)

// Alphametics alphametrics solution
type Alphametics struct {
	puzzle    string
	equation  [][]byte
	variables [26]byte
	digits    [10]byte
	letters   []byte
	first     [26]bool
}

// LetterExists check if letter LetterExists in equation
func (alpha *Alphametics) LetterExists(letter byte) bool {
	return alpha.variables[letter] != EmptyLetter
}

// LetterValue get value of a letter
func (alpha *Alphametics) LetterValue(letter byte) int {
	return int(alpha.variables[letter])
}

// IsSet check if letter value is set
func (alpha *Alphametics) IsSet(letter byte) bool {
	return alpha.variables[letter] != NonDigit
}

// SetLetterValue set value for a letter
func (alpha *Alphametics) SetLetterValue(letter byte, val byte) {
	// Check if last value was set
	if alpha.variables[letter] < NonDigit {
		// Reset digit value
		alpha.digits[alpha.variables[letter]] = NonLetter
	}
	// Check if its a valid value
	if val < NonDigit {
		// Save digit to letter map
		alpha.digits[val] = letter
	}
	// Save letter value
	alpha.variables[letter] = val
}

// IsFirstDigit check if this is the first digit
func (alpha *Alphametics) IsFirstDigit(letter byte) bool {
	return alpha.first[letter]
}

// FirstDigit set the digit as first
func (alpha *Alphametics) FirstDigit(letter byte) {
	alpha.first[letter] = true
}

// IsUnique check if this letter value is already used
func (alpha *Alphametics) IsUnique(val int) bool {
	return alpha.digits[val] == NonLetter
}

// Number convert part of equation to a number
func (alpha *Alphametics) Number(n int) int {
	val := 0
	arr := alpha.equation[n]
	for i := 0; i < len(arr); i++ {
		val = val*10 + alpha.LetterValue(arr[i])
	}
	return val
}

// New get a new alphametrics
func New(puzzle string) *Alphametics {
	var digits [10]byte
	var variables [26]byte
	// Initiate Default value
	for i := 0; i < len(variables); i++ {
		variables[i] = EmptyLetter
	}
	for i := 0; i < len(digits); i++ {
		digits[i] = NonLetter
	}
	return &Alphametics{
		puzzle:    puzzle,
		digits:    digits,
		variables: variables}
}

// Parse parse puzzle and get equation parts
func (alpha *Alphametics) Parse() error {
	max := 0
	maxi := 0
	var equation []byte
	for _, ch := range alpha.puzzle {
		if unicode.IsLetter(ch) {
			b := byte(ch - 'A')
			// Check if this is the first digit
			if equation == nil {
				alpha.FirstDigit(b)
			}

			// check if its a unique digit
			if !alpha.LetterExists(b) {
				alpha.SetLetterValue(b, NonDigit)
				alpha.letters = append(alpha.letters, b)
			}
			// Update part of equation
			equation = append(equation, b)
		} else if equation != nil {
			// max digits of numbers
			if len(equation) > max {
				max = len(equation)
				maxi = len(alpha.equation)
			}
			// update equation
			alpha.equation = append(alpha.equation, equation)
			equation = nil
		}
	}
	// Remaining part of eqaution
	if equation != nil {
		alpha.equation = append(alpha.equation, equation)
		equation = nil
	}
	// Max 10 letters are supported
	if len(alpha.letters) > 10 || len(alpha.letters) < 1 {
		return errors.New("More than 10 letters")
	}
	// Atleast 2 parts should be present for a valid equation
	if len(alpha.equation) < 2 {
		return errors.New("Min two parts should be in equation")
	}
	// If there is a carry it can only be 1
	if maxi == len(alpha.equation)-1 {
		alpha.variables[alpha.equation[maxi][0]] = 1
	}
	return nil
}

// Solution save solution for returning
func (alpha *Alphametics) Solution() map[string]int {
	solution := make(map[string]int)
	for _, b := range alpha.letters {
		r := rune(b + 'A')
		solution[string(r)] = alpha.LetterValue(b)
	}
	return solution
}

// Solve solve current equations, when n variables has a value assigned
func (alpha *Alphametics) Solve(n int) bool {
	var result bool
	// Check if each variable has a value
	if n == len(alpha.letters) {
		// all variables are assigned, avaluate equation
		if alpha.Evaluate() {
			return true
		}
		return false
	}
	letter := alpha.letters[n]
	// Check if already set
	if alpha.IsSet(letter) {
		return alpha.Solve(n + 1)
	}
	// Assign each value and check
	for i := 0; i <= 9; i++ {
		// first digit can not be zero, and digits must be unique
		if (i == 0 && alpha.IsFirstDigit(letter)) || !alpha.IsUnique(i) {
			continue
		}
		// Set this value and solve again
		alpha.SetLetterValue(letter, byte(i))
		if alpha.Solve(n + 1) {
			return true
		}
	}
	// Reset this value
	alpha.SetLetterValue(letter, NonDigit)
	return result
}

// Evaluate equations lhs and rhs using current map
func (alpha *Alphametics) Evaluate() bool {
	val := 0
	i := 0
	for ; i < len(alpha.equation)-1; i++ {
		val += alpha.Number(i)
	}
	if alpha.Number(i) != val {
		return false
	}
	return true
}

// Solve solve the alphametrics puzzle
func Solve(puzzle string) (map[string]int, error) {
	//fmt.Println("##", str)
	alpha := New(puzzle)
	if err := alpha.Parse(); err != nil {
		return nil, err
	}
	if alpha.Solve(0) {
		return alpha.Solution(), nil
	}
	return nil, errors.New("Not unique solution")
}
