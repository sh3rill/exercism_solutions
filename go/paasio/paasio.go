// Package paasio provides interfaces to save stats for read and write
package paasio

import (
	"io"
	"sync"
)

// Writer Writer Object implements WriteCounter
type Writer struct {
	writer     io.Writer
	mutex      *sync.Mutex
	writeCount int
	byteCount  int64
}

// Reader Reader Object implemnets ReadCounter
type Reader struct {
	reader    io.Reader
	mutex     *sync.Mutex
	readCount int
	byteCount int64
}

// ReaderWriter reader and writer object
type ReaderWriter struct {
	WriteCounter
	ReadCounter
}

// ReaderWriterIO reader writer object
type ReaderWriterIO interface {
	io.Reader
	io.Writer
}

// NewWriteCounter return WriteCounter object
func NewWriteCounter(writer io.Writer) WriteCounter {
	m := &sync.Mutex{}
	return &Writer{writer, m, 0, 0}
}

// NewReadCounter return ReadCounter object
func NewReadCounter(reader io.Reader) ReadCounter {
	m := &sync.Mutex{}
	return &Reader{reader, m, 0, 0}
}

// NewReadWriteCounter return reader writer counter
func NewReadWriteCounter(rw ReaderWriterIO) ReadWriteCounter {
	r := NewReadCounter(rw)
	w := NewWriteCounter(rw)
	return &ReaderWriter{w, r}
}

// Write write data using io.writer and save stats
func (p *Writer) Write(data []byte) (int, error) {
	n, err := p.writer.Write(data)
	if err != nil {
		return n, err
	}
	p.mutex.Lock()
	p.writeCount++
	p.byteCount += int64(n)
	p.mutex.Unlock()
	return n, nil
}

// WriteCount get write stats
func (p *Writer) WriteCount() (n int64, nops int) {
	p.mutex.Lock()
	n = p.byteCount
	nops = p.writeCount
	p.mutex.Unlock()
	return n, nops
}

// ReadCount get reader stats
func (p *Reader) ReadCount() (n int64, nops int) {
	p.mutex.Lock()
	n = p.byteCount
	nops = p.readCount
	p.mutex.Unlock()
	return n, nops
}

// Read read data using io.redaer and save stats
func (p *Reader) Read(data []byte) (int, error) {
	n, err := p.reader.Read(data)
	if err != nil {
		return n, err
	}
	p.mutex.Lock()
	p.readCount++
	p.byteCount += int64(n)
	p.mutex.Unlock()
	return n, nil
}
