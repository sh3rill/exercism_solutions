package palindrome

import (
	"errors"
	"math"
	"strconv"
)

// Product product
type Product struct {
	Product        int
	Factorizations [][2]int
}

// isPalindrome check if number is palindrome
func isPalindrome(no int) bool {
	if no < 10 {
		return true
	}
	s := strconv.Itoa(no)
	i := len(s)
	j := 0
	if i&01 == 0 {
		j = i / 2
		i = j - 1
	} else {
		j = i / 2
		i = j
	}
	for i >= 0 && j < len(s) {
		if s[i] != s[j] {
			return false
		}
		i--
		j++
	}
	return true
}

// factors get feactors between min and max
func factors(n, fmin, fmax int) [][2]int {
	var f [][2]int
	var m int
	if n > 0 {
		f = make([][2]int, 0, n)
		m = int(math.Min(math.Sqrt(float64(n)), float64(fmax)))
	} else {
		f = make([][2]int, 0, -n)
		m = int(math.Min(math.Sqrt(float64(-n)), float64(fmax)))
	}

	for i := fmin; i <= m; i++ {
		if i == 0 {
			continue
		}
		if n%i == 0 {
			a := [2]int{}
			a[0], a[1] = i, n/i
			if a[0] <= a[1] && a[0] >= fmin && a[0] <= fmax &&
				a[1] >= fmin && a[1] <= fmax {
				f = append(f, a)
			}
		}
	}

	return f
}

// New create a product
func New(n, fmin, fmax int) Product {
	f := factors(n, fmin, fmax)
	return Product{n, f}
}

// Products get the min and max palindrome product
func Products(fmin, fmax int) (Product, Product, error) {
	if fmax < fmin {
		return Product{}, Product{}, errors.New("fmin > fmax")
	}

	found := false
	max := math.MinInt32
	min := math.MaxInt32

	for i := fmin; i <= fmax; i++ {
		for j := i; j <= fmax; j++ {
			n := i * j
			if (n < 0 && isPalindrome(-n)) || isPalindrome(n) {
				found = true
				if n < min {
					min = n
				}
				if n > max {
					max = n
				}
			}

		}
	}
	if found {
		if min == max {
			p := New(min, fmin, fmax)
			return p, p, nil
		}
		return New(min, fmin, fmax), New(max, fmin, fmax), nil
	}
	return Product{}, Product{}, errors.New("no palindromes")
}
