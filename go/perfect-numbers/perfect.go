package perfect

import (
	"errors"
	"math"
)

type Classification string

var ErrOnlyPositive = errors.New("Only positive no")

const (
	ClassificationDeficient = "deficient"
	ClassificationPerfect   = "prefect"
	ClassificationAbundant  = "abundant"
)

func factors(n int64) []int64 {
	m := int64(math.Sqrt(float64(n)))
	f := make([]int64, 1, n)
	f[0] = 1
	var i int64
	for i = 2; i <= m; i++ {
		if n%i == 0 {
			f = append(f, i)
			j := n / i
			if i != j {
				f = append(f, j)
			}
		}
	}
	return f
}

// Classify classify number based on the factors
func Classify(n int64) (Classification, error) {
	if n <= 0 {
		return "", ErrOnlyPositive
	} else if n == 1 {
		return ClassificationDeficient, nil
	}
	f := factors(n)

	var s int64
	for i := 0; i < len(f); i++ {
		s += f[i]
	}
	if s < n {
		return ClassificationDeficient, nil
	} else if s == n {
		return ClassificationPerfect, nil
	}
	return ClassificationAbundant, nil
}
