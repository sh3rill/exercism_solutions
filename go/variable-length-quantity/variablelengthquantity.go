package variablelengthquantity

import (
	"errors"
)

func decode(data []byte) uint32 {
	var n uint32
	for _, m := range data {
		x := m & 0x7F
		n = n<<7 | uint32(x)
	}
	return n
}

// DecodeVarint decode variable length quantity
func DecodeVarint(data []byte) ([]uint32, error) {
	var numbers []uint32
	j := 0
	for i, m := range data {
		if m&0x80 == 0 {
			n := decode(data[j : i+1])
			numbers = append(numbers, n)
			j = i + 1
		} else if i == len(data)-1 {
			return nil, errors.New("")
		}
	}

	return numbers, nil
}

func reverse(data []byte) {
	for i, j := 0, len(data)-1; i < j; i, j = i+1, j-1 {
		data[i], data[j] = data[j], data[i]
	}
}

func encode(n uint32) []byte {
	var data []byte
	for n > 0x7F {
		m := n & 0x7F
		n >>= 7
		// Do not set this if this is only byte
		if len(data) != 0 {
			m |= 0x80
		}
		data = append(data, byte(m))
	}

	// Check if this is the only byte
	if len(data) != 0 {
		n = n | 0x80
	}

	data = append(data, byte(n))

	// We need to go from most significant byte to least, so reverse
	reverse(data)
	return data
}

// EncodeVarint encode variable length quantity
func EncodeVarint(numbers []uint32) []byte {
	var data []byte
	for _, number := range numbers {
		d := encode(number)
		data = append(data, d...)
	}
	return data
}
