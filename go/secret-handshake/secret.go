package secret

const CODE_COUNT = 4

var code = [4]string{
	"wink",
	"double blink",
	"close your eyes",
	"jump",
}

// reverse reverse a list
func reverse(list []string) {
	for i := len(list)/2 - 1; i >= 0; i-- {
		opp := len(list) - 1 - i
		list[i], list[opp] = list[opp], list[i]
	}
}

// Handshake convert val to codes
func Handshake(val uint) []string {
	list := make([]string, 0, CODE_COUNT)
	var i uint
	for i = 0; i < CODE_COUNT; i++ {
		if val&(1<<i) != 0 {
			list = append(list, code[i])
		}
	}
	if val&(1<<i) != 0 {
		reverse(list)
	}
	return list
}
