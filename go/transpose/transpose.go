package transpose

// Transpose get transpose of a string
func Transpose(input []string) []string {
	var l int
	for _, s := range input {
		if len(s) > l {
			l = len(s)
		}
	}
	out := make([]string, l)
	for i := range out {
		col := make([]byte, len(input))
		for j := 0; j < len(input); j++ {
			if i < len(input[j]) {
				col[j] = input[j][i]
			} else {
				col[j] = byte(' ')
			}
		}
		// Remove right pad for last row
		if i == l-1 {
			j := len(col) - 1
			for ; j >= 0 && col[j] == ' '; j-- {

			}
			col = col[:j+1]

		}
		out[i] = string(col)
	}
	return out
}
