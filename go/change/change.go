package change

import (
	"errors"
	"fmt"
	"math"
)

func reverse(arr []int) {
	for i, j := 0, len(arr)-1; i < j; i, j = i+1, j-1 {
		fmt.Println("Reverse", i, j)
		arr[i], arr[j] = arr[j], arr[i]
	}
}

func greedy(coins []int, target int) ([]int, error) {
	var change []int
	if target < 0 {
		return nil, errors.New("")
	}
	for i := len(coins) - 1; i >= 0 && target > 0; i-- {
		fmt.Println(i, ")", "Coin:", coins[i], "target:", target)
		if target >= coins[i] {
			count := target / coins[i]
			for j := 0; j < count; j++ {
				change = append(change, coins[i])
			}
			target %= coins[i]
		} else {
			fmt.Println("Ignore", coins[i])
		}
	}
	if target != 0 {
		return nil, errors.New("")
	}
	reverse(change)
	return change, nil
}

func dp(coins []int, target int) ([]int, error) {
	counts := make([]int, target+1)

	for i := 1; i < len(counts); i++ {
		counts[i] = math.MaxInt32
	}

	change := make([]int, target+1)
	for p := 1; p <= target; p++ {
		for _, coin := range coins {
			if coin <= p {
				count := counts[p-coin]
				if count != math.MaxInt32 && 1+count < counts[p] {
					counts[p] = 1 + count
					// update coin
					change[p] = coin
				}
			}
		}
	}
	var arr []int
	for target > 0 {
		if change[target] == 0 {
			// We could not find a valid combination for
			return nil, errors.New("Not Possible")
		}
		arr = append(arr, change[target])
		target -= change[target]
	}

	return arr, nil
}

// Change fnd change with coins
func Change(coins []int, target int) ([]int, error) {
	//return greedy(coins, target)
	if target < 0 {
		return nil, errors.New("Negative value")
	}
	if target == 0 {
		return []int{}, nil
	}
	return dp(coins, target)
}
