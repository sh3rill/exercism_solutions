package summultiples

var exists = struct{}{}

func SumMultiples(limit int, divisors ...int) int {
	s := 0
	set := map[int]struct{}{}
	for _, divisor := range divisors {
		for val := divisor; val < limit; val += divisor {
			if _, ok := set[val]; !ok {
				set[val] = exists
				s += val
			}

		}
	}

	return s
}
