func multiplesSumOf(limit, divisor int) int {
	n := (limit - 1) / divisor
	n = n * (n + 1) * divisor / 2
	return n
}

func gcd(a, b int) int {
	if a == 0 {
		return b
	}
	return gcd(b%a, a)
}

func lcm(a int, b ...int) int {
	l := a
	for _, val := range b {
		l = l * val / gcd(l, val)
	}
	return l
}

func lcmMultiplesSum(limit int, args []int) int {
	s := 0
	for n := 1; n <= len(args); n++ {
		for i := 0; i < len(args)-n; i++ {
			l := 0
			l = lcm(args[i], args[i+1:i+1+n]...)
			d := multiplesSumOf(limit, l)
			s += d
		}
	}
	return s
}

// SumMultiples get the sum of all multiples
func SumMultiples(limit int, divisors ...int) int {
	s := 0
	if limit <= 1 {
		return 0
	}
	for _, divisor := range divisors {
		if divisor > 0 {
			s += multiplesSumOf(limit, divisor)
		}
	}
	// all multiples of hcf has been inluded multiple times
	d := lcmMultiplesSum(limit, divisors)
	s -= d
	return s
}
