package say

var digits = []string{
	"zero",
	"one",
	"two",
	"three",
	"four",
	"five",
	"six",
	"seven",
	"eight",
	"nine",
}

var tens = []string{
	"ten",
	"eleven",
	"twelve",
	"thirteen",
	"fourteen",
	"fifteen",
	"sixteen",
	"seventeen",
	"eighteen",
	"nineteen",
}

var decade = []string{
	"",
	"ten",
	"twenty",
	"thirty",
	"forty",
	"fifty",
	"sixty",
	"seventy",
	"eighty",
	"ninety",
}

var tenMultiples = []string{
	"thousand",
	"million",
	"billion",
}

func hundreds(n int) string {
	if n < 1000 {
		m := n / 100
		s := ""
		if m > 0 {
			s = digits[m] + " hundred"
			n %= 100
			if n > 0 {
				s += " "
			}
		}
		m = n / 10
		if m > 1 {
			s += decade[m]
			n %= 10
			if n > 0 {
				s += "-" + digits[n]
			}
		} else if m == 1 {
			s += tens[n%10]
		} else if n > 0 {
			s += digits[n]
		}

		return s
	}
	println("Number can not be graeter than thousand", n)

	return ""
}

// Say convert number to words
func Say(n int64) (string, bool) {
	if n < 0 || n >= 1000000000000 {
		return "", false
	}
	var s string
	if n < 10 {
		s = digits[n]
	} else if n < 1000 {
		s = hundreds(int(n))
	} else {
		var m int64 = 1000000000
		for i := len(tenMultiples) - 1; i >= 0; i-- {
			j := int(n / m)
			if j > 0 {
				s = s + hundreds(j) + " " + tenMultiples[i]
				n %= m
				if n > 0 {
					s += " "
				}
			}
			m /= 1000
		}
		s = s + hundreds(int(n))
	}
	return s, true
}
