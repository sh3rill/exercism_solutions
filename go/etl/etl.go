package etl

import "strings"

func Transform(m map[int][]string) map[string]int {
	t := map[string]int{}
	for key, val := range m {
		for _, s := range val {
			t[strings.ToLower(s)] = key
		}
	}
	return t
}
