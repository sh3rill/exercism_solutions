package rotationalcipher

import "unicode"

func rotate(ch rune, key rune) rune {
	if ch >= 'A' && ch <= 'Z' {
		ch = ((ch - 'A' + key) % 26) + 'A'
	} else if ch >= 'a' && ch <= 'z' {
		ch = ((ch - 'a' + key) % 26) + 'a'
	}
	return ch
}

// RotationalCipher rotate text by given key
func RotationalCipher(plain string, key int) string {
	key %= 26
	if key == 0 {
		return plain
	}
	keyR := rune(key)
	plainArr := make([]rune, len(plain))
	for i, ch := range plain {
		if unicode.IsLetter(ch) {
			ch = rotate(ch, keyR)
		}
		plainArr[i] = ch
	}

	return string(plainArr)
}
