package pangram

// IsPangram check is sentence is pangram
func IsPangram(sentence string) bool {
	m := map[rune]struct{}{}
	for _, v := range sentence {
		if v >= 'A' && v <= 'Z' {
			v -= 'A'
			v += 'a'
		}
		if v >= 'a' && v <= 'z' {
			m[v] = struct{}{}
		}
	}
	for i := 'a'; i <= 'z'; i++ {
		if _, ok := m[i]; !ok {
			return false
		}
	}
	return true
}
