package flatten

import "fmt"

// Flatten flatten list of nested lists
func Flatten(args ...interface{}) []interface{} {
	list := make([]interface{}, 0, len(args))
	for _, arg := range args {
		switch arg.(type) {
		case []interface{}:
			l := Flatten(arg.([]interface{})...)
			list = append(list, l...)
		case interface{}:
			list = append(list, arg.(interface{}))
		case nil:
		default:
			fmt.Println("Error", arg)
		}
	}
	return list
}
