package isogram

func IsIsogram(s string) bool {
	m := make(map[byte]bool)
	l := len(s)
	for i := 0; i < l; i++ {
		ch := s[i]

		if ch >= 'a' && ch <= 'z' {
			ch = ch - 'a' + 'A'
		}
		if ch >= 'A' && ch <= 'Z' {
			if m[ch] {
				return false
			}
			m[ch] = true
		}
	}
	return true
}
