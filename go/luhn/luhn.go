package luhn

import "strings"

// Valid Check if a given string of numbers is valid luhn
func Valid(s string) bool {
	s = strings.TrimSpace(s)
	l := len(s)
	if l <= 1 {
		return false
	}
	sum := 0
	n := 0
	skip := true
	for i := l - 1; i >= 0; i-- {
		ch := s[i]
		switch {
		case ch >= '0' && ch <= '9':
			n = int(ch - '0')
			if !skip {
				n = n * 2
				if n > 9 {
					n -= 9
				}
			}

			sum += n

			skip = !skip
		case ch == ' ' || ch == '\r' || ch == '\t':

		default:
			return false
		}
	}
	if sum%10 == 0 {
		return true
	}
	return false
}
