package bookstore

import "math"

// CostPerBook cost per book is standard
const CostPerBook = 800

var discounts = [...]int{0, 5, 10, 20, 25}

func groupCost(size int) int {
	return CostPerBook * size * (100 - discounts[size-1]) / 100
}

func cost(books [5]int, price int) int {
	count := 0
	minPrice := math.MaxInt32
	// One by one add one book in this set and check the price
	for i := 0; i < len(books); i++ {
		// conitnue if no copies book exists
		if books[i] == 0 {
			continue
		}
		// Distict Books in this set
		count++
		// Book is part of current set so remove from this one
		books[i]--
		// Remove one book from the set and check the cost
		p := cost(books, price+groupCost(count))
		// Update we are able to reduce the cost
		if p < minPrice {
			minPrice = p
		}
	}
	// If there were no books in this set then return price of previous set
	if count == 0 {
		return price
	}
	// Return price if this this set
	return minPrice
}

// Cost cost of all books
func Cost(books []int) int {
	if len(books) == 0 {
		return 0
	}

	count := 0
	max := math.MinInt32
	var copies = [5]int{}
	for i := 0; i < len(books); i++ {
		j := books[i] - 1
		copies[j]++
		if copies[j] > max {
			max = copies[j]
		}
		count++
	}
	// If we have only one copy per book then only one group
	if max == 1 {
		return groupCost(count)
	}
	return cost(copies, 0)
}
