package house

var nouns = []string{
	"house that Jack built.",
	"malt",
	"rat",
	"cat",
	"dog",
	"cow with the crumpled horn",
	"maiden all forlorn",
	"man all tattered and torn",
	"priest all shaven and shorn",
	"rooster that crowed in the morn",
	"farmer sowing his corn",
	"horse and the hound and the horn",
}
var verbs = []string{
	"lay in",
	"ate",
	"killed",
	"worried",
	"tossed",
	"milked",
	"kissed",
	"married",
	"woke",
	"kept",
	"belonged to",
}

func Verse(count int) string {
	var str string
	count--
	if count >= 0 && count < len(nouns) {
		str = "This is the " + nouns[count]
		if count > 0 {
			for i := count - 1; i >= 0; i-- {
				str += "\nthat " + verbs[i] + " the " + nouns[i]
			}
		}

	}
	return str
}

func Song() string {
	var str string
	for i := 1; i <= len(nouns); i++ {
		s := Verse(i)
		if str == "" {
			str = s
		} else {
			str += "\n\n" + s
		}
	}
	return str
}
