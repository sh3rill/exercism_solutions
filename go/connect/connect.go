package connect

import (
	"errors"
	"fmt"
)

// Direction movement of direction
type Direction int

// As this is a rhombus going upward movement upright and down left is not possible
const (
	Up Direction = iota + 1
	UpLeft
	Right
	Down
	DownRight
	Left
	DirectionStart = Up
	DirectionEnd   = Left
)

// Marker marker to track current position
type Marker struct {
	x, y  int
	board [][]byte
}

func (m Marker) String() string {
	return fmt.Sprintf("{%d:%d}->{%d, %d}", m.x, m.y, len(m.board), len(m.board[m.y]))
}

// NewMarker Create a new Marker
func NewMarker(x, y int, board [][]byte) *Marker {
	return &Marker{x, y, board}
}

// Current player
func (m *Marker) Current() byte {
	return m.board[m.y][m.x]
}

// Move move marker in the direction
func (m *Marker) Move(dir Direction) bool {
	switch dir {
	case Up:
		m.y++
	case UpLeft:
		m.y++
		m.x--
	case Right:
		m.x++
	case Down:
		m.y--
	case DownRight:
		m.x++
		m.y--
	case Left:
		m.x--
	}
	if !m.IsValid() {
		return false
	}
	return true
}

// Set set marker
func (m *Marker) Set(x, y int) {
	m.x, m.y = x, y
}

// Visited set position as visited
func (m *Marker) Visited() byte {
	data := m.board[m.y][m.x]
	m.board[m.y][m.x] = '.'
	return data
}

// IsValid check if this is a valid marker position
func (m *Marker) IsValid() bool {
	return m.y >= 0 && m.y < len(m.board) && m.x >= 0 && m.x < len(m.board[m.y])
}

// Traverse traverse board with this marker
func (m *Marker) Traverse(gameEnded func(m *Marker) bool) (bool, error) {
	// Check if game has ended
	if gameEnded(m) {
		return true, nil
	}
	// Save this player and set position to visited
	player := m.Visited()
	// Try to find winner in all direction
	for side := DirectionStart; side <= DirectionEnd; side++ {
		// Save last positions
		x, y := m.x, m.y
		// Move and check if we have same player
		if m.Move(side) && m.Current() == player {
			status, err := m.Traverse(gameEnded)
			if err != nil {
				return false, err
			}
			// Check we reached limit
			if status {
				return true, nil
			}
		}
		// No result from that side
		// Move back
		m.Set(x, y)
	}
	return false, nil
}

// BottomUpWinner Check board bottom up for winner
func BottomUpWinner(board []string) (string, error) {
	// Check top to Bottom
	lastch := '.'
	// Create new Marker
	boardCopy := make([][]byte, len(board))
	for i := 0; i < len(board); i++ {
		boardCopy[i] = make([]byte, len(board[i]))
		data := []byte(board[i])
		copy(boardCopy[i], data)
	}
	m := NewMarker(0, 0, boardCopy)

	// Check from Top To Bottom
	for i, ch := range board[0] {
		// If there is only One Row, Any one can be a Winner
		// Check we have not tested this cell before
		if ch != '.' && ch != lastch {
			// We Found a winner
			if len(board) == 1 {
				return string(ch), nil
			}
			// Create new marker
			m.Set(i, 0)
			status, err := m.Traverse(func(m *Marker) bool {
				return m.y == len(m.board)-1
			})
			if err != nil {
				return "", err
			}
			if status {
				return string(ch), nil
			}
		}
		lastch = ch
	}
	return "", nil
}

// LeftRightWinner check board from left to right
func LeftRightWinner(board []string) (string, error) {
	var lastch byte = '.'
	boardCopy := make([][]byte, len(board))
	for i := 0; i < len(board); i++ {
		boardCopy[i] = make([]byte, len(board[i]))
		data := []byte(board[i])
		copy(boardCopy[i], data)
	}
	m := NewMarker(0, 0, boardCopy)
	// Check Side
	for i, line := range board {
		ch := line[0]
		if ch != '.' && ch != lastch {
			if len(board[i]) == 1 {
				return string(ch), nil
			}
			// Move to this position
			m.Set(0, i)
			// Try to find winner from this position
			status, err := m.Traverse(func(m *Marker) bool {
				return m.x == len(m.board[m.y])-1
			})
			if err != nil {
				return "", err
			}
			if status {
				return string(ch), nil
			}
		}
		lastch = ch
	}
	return "", nil
}

// ResultOf get result of board game
func ResultOf(board []string) (string, error) {
	if len(board) < 1 {
		return "", errors.New("Empty Board")
	}
	// Check if board is valid
	width := 0
	for i, line := range board {
		if i == 0 && len(line) > 0 {
			width = len(line)
		} else if len(line) != width {
			return "", errors.New("Row length is wrong")
		}
		for _, ch := range line {
			if ch != '.' && ch != 'O' && ch != 'X' {
				return "", errors.New("Wrong character")
			}
		}
	}

	// Find winner from bottom up
	winner, err := BottomUpWinner(board)
	if err != nil {
		return "", err
	}
	if winner != "" {
		return winner, nil
	}
	// Find winner from left to right
	winner, err = LeftRightWinner(board)
	if err != nil {
		return "", err
	}
	return winner, nil
}
