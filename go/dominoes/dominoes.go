package dominoes

import "fmt"

// Dominoe single dominoe element
type Dominoe [2]int

// Chain dominoe chain
type Chain struct {
	input    []Dominoe
	dominoes []*Dominoe
	chain    []*Dominoe
	start    int
	end      int
	size     int
}

func (d *Dominoe) String() string {
	return fmt.Sprintf("{%d,%d}", d[0], d[1])
}

func (c *Chain) String() string {
	s := ""
	for i := 0; i < c.size; i++ {
		s += c.chain[i].String()
	}
	return fmt.Sprintf("Start:%d, End:%d, Chain:%s", c.start, c.end, s)
}

// Flip flip this domino
func (d *Dominoe) Flip() {
	d[0], d[1] = d[1], d[0]
}

// CanFlip check if we can flip this element, if both fields are different
func (d *Dominoe) CanFlip() bool {
	return d[0] != d[1]
}

// Start dominoe start
func (d *Dominoe) Start() int {
	return d[0]
}

// End dominoe end
func (d *Dominoe) End() int {
	return d[1]
}

// New get new chain from the input
func New(input []Dominoe) *Chain {
	chain := make([]*Dominoe, len(input))
	dominoes := make([]*Dominoe, len(input))
	for i := 0; i < len(input); i++ {
		dominoes[i] = &input[i]
	}
	return &Chain{dominoes: dominoes, chain: chain, input: input}
}

// IsValid check if this a valid chain
func (c *Chain) IsValid() bool {
	return c.size == 0 || c.start == c.end
}

// Flip flip a domino in the list
func (c *Chain) Flip(n int) {
	if n < len(c.dominoes) {
		c.dominoes[n].Flip()
		if n == 0 {
			c.start = c.dominoes[0].Start()
		}
		if len(c.dominoes) == n+1 {
			c.end = c.dominoes[n].End()
		}
	}
}

// Update update chain after any change
func (c *Chain) Update() {
	if c.size == 0 {
		c.start, c.end = -1, -1
		return
	} else if c.size == 1 {
		c.start, c.end = c.chain[0].Start(), c.chain[0].End()
	}
	c.end = c.chain[c.size-1].End()
}

// Add add an element in the chain
func (c *Chain) Add(n int) {
	c.chain[c.size] = c.dominoes[n]
	c.size++
	c.Update()
}

// IsChainable check if this element can be added
func (c *Chain) IsChainable(n int) bool {
	if c.size == 0 || c.end == c.dominoes[n].Start() {
		return true
	}
	return false
}

// IsReverseChainable check if this element can be added after a reverse operation
func (c *Chain) IsReverseChainable(n int) bool {
	if c.size == 0 || c.end == c.dominoes[n].End() {
		return true
	}
	return false
}

// Append append an elemet in the chain
func (c *Chain) Append(n int) bool {
	d := c.dominoes[n]
	if c.IsChainable(n) {
		c.Add(n)
		c.dominoes[n] = nil
		if c.Create() {
			return true
		}
		c.size--
		c.Update()
		c.dominoes[n] = d
	} else if c.IsReverseChainable(n) && d.CanFlip() {
		//fmt.Println("Flip", c.dominoes[n])
		d.Flip()
		c.Add(n)
		c.dominoes[n] = nil
		if c.Create() {
			return true
		}
		c.size--
		c.Update()
		c.dominoes[n] = d
		d.Flip()
	}
	return false
}

// Create create a chain from remaining input
func (c *Chain) Create() bool {
	var result = true
	for i := 0; i < len(c.dominoes); i++ {
		if c.dominoes[i] != nil {
			if c.Append(i) {
				return true
			}
			result = false
		}
	}
	return result
}

// Array convert chain to array
func (c *Chain) Array() []Dominoe {
	d := make([]Dominoe, len(c.chain))
	for i := 0; i < len(c.chain); i++ {
		d[i] = *c.chain[i]
	}
	return d
}

// MakeChain make a chain from input dominoe list
func MakeChain(input []Dominoe) (chain []Dominoe, ok bool) {
	if len(input) == 0 {
		return nil, true
	}
	c := New(input)
	if c.Create() && c.IsValid() {
		//fmt.Println(c)
		return c.Array(), true
	}
	return nil, false
}
