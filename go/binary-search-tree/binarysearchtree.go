package binarysearchtree

import (
	"fmt"
)

type SearchTreeData struct {
	left  *SearchTreeData
	data  int
	right *SearchTreeData
}

// Bst create a bst
func Bst(val int) *SearchTreeData {
	return &SearchTreeData{data: val}
}

// String function
func (t *SearchTreeData) String() string {
	return fmt.Sprintf("{%s %d %s }", t.left, t.data, t.right)
}

// Insert insert value in bst
func (t *SearchTreeData) Insert(val int) {
	if t.data >= val {
		if t.left == nil {
			t.left = Bst(val)
		} else {
			t.left.Insert(val)
		}
	} else {
		if t.right == nil {
			t.right = Bst(val)
		} else {
			t.right.Insert(val)
		}
	}
}

// MapString run given function returning string on all nodes
func (t *SearchTreeData) MapString(f func(int) string) []string {
	strs := make([]string, 0)
	if t.left != nil {
		s := t.left.MapString(f)
		strs = append(strs, s...)
	}
	s := f(t.data)
	strs = append(strs, s)
	if t.right != nil {
		s := t.right.MapString(f)
		strs = append(strs, s...)
	}
	return strs
}

// MapInt run given function on all nodes
func (t *SearchTreeData) MapInt(f func(int) int) []int {
	ints := make([]int, 0)
	if t.left != nil {
		i := t.left.MapInt(f)
		ints = append(ints, i...)
	}
	i := f(t.data)
	ints = append(ints, i)
	if t.right != nil {
		i := t.right.MapInt(f)
		ints = append(ints, i...)
	}
	return ints
}
