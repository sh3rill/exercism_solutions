package dna

import "errors"

// Histogram is a mapping from nucleotide to its count in given DNA.
// Choose a suitable data type.
type Histogram map[byte]int

// DNA is a list of nucleotides. Choose a suitable data type.
type DNA string

var nucleotides = map[byte]struct{}{
	'A': struct{}{},
	'T': struct{}{},
	'G': struct{}{},
	'C': struct{}{},
}

// Count counts number of occurrences of given nucleotide in given DNA.
//
// This is a method on the DNA type. A method is a function with a special receiver argument.
// The receiver appears in its own argument list between the func keyword and the method name.
// Here, the Count method has a receiver of type DNA named d.
func (d DNA) Count(nucleotide byte) (int, error) {
	if _, ok := nucleotides[nucleotide]; !ok {
		return -1, errors.New("Unknown nucleotide : " + string(nucleotide))
	}
	h, err := d.Counts()
	if err != nil {
		return -1, err
	}
	return h[nucleotide], nil
}

// Counts generates a histogram of valid nucleotides in the given DNA.
// Returns an error if d contains an invalid nucleotide.
func (d DNA) Counts() (Histogram, error) {
	var h = Histogram{
		'A': 0,
		'C': 0,
		'T': 0,
		'G': 0,
	}
	for _, ch := range d {
		if _, ok := h[byte(ch)]; !ok {
			return nil, errors.New("Unknown nucleotide : " + string(ch))
		}
		h[byte(ch)]++
	}

	return h, nil
}
