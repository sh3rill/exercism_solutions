package strand

// ToRNA convert DNA string to RNA
func ToRNA(dna string) string {
	r := make([]rune, len(dna))
	for i, val := range dna {
		switch val {
		case 'C':
			r[i] = 'G'
		case 'G':
			r[i] = 'C'
		case 'T':
			r[i] = 'A'
		case 'A':
			r[i] = 'U'
		default:
			println("Unexpected input", string(val))
		}
	}
	return string(r)
}
