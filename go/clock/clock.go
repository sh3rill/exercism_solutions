package clock

import (
	"fmt"
)

// Clock clock object
type Clock struct {
	hour int
	min  int
}

// New create a new clock object from hour and minutes
func New(h, m int) Clock {
	if m < 0 {
		m = -m
		t := m / 60
		h -= t
		m %= 60
		if m > 0 {
			h--
			m = 60 - m
		}
	} else {
		t := m / 60
		h += t
		m %= 60
	}

	if h < 0 {
		h = -h
		h = h % 24
		if h > 0 {
			h = 24 - h
		}
	} else {
		h = h % 24
	}

	h %= 24
	return Clock{h, m}
}

// Add add minutes in clock time and return a new clock
func (c Clock) Add(min int) Clock {
	return New(c.hour, c.min+min)
}

// String convert clock to string
func (c Clock) String() string {
	s := fmt.Sprintf("%02d:%02d", c.hour, c.min)
	return s
}
