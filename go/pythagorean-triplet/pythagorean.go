package pythagorean

import "math"

// Triplet pythogorean triplet
type Triplet [3]int

// Perimeter get the perimeter of a triplet
func (t Triplet) Perimeter() int {
	return t[0] + t[1] + t[2]
}

// Range get a list of triplets with in the range
func Range(min, max int) []Triplet {
	var tlist []Triplet
	for i := min; i <= max; i++ {
		for j := i; j <= max; j++ {
			k2 := i*i + j*j
			k := int(math.Sqrt(float64(k2)))
			if k*k == k2 && k <= max {
				tlist = append(tlist, Triplet{i, j, k})
			}
		}
	}
	return tlist
}

// Sum get the list of all triplets who have this sum
func Sum(p int) []Triplet {
	var tlist []Triplet
	list := Range(1, p)
	for _, t := range list {
		if t.Perimeter() == p {
			tlist = append(tlist, t)
		}
	}
	return tlist
}
