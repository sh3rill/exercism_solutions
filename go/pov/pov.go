package pov

import "fmt"

type node struct {
	label    string
	parent   *node
	children []*node
}

// Graph graph
type Graph struct {
	nodes map[string]*node
}

// New get a new graph
func New() *Graph {
	return &Graph{nodes: map[string]*node{}}
}

// root get root of the path that passes through this node
func (n *node) root() *node {
	n1 := n.parent
	if n1 == nil {
		return n
	}
	for n1 != nil {
		n1 = n1.parent
	}
	return n1
}

// String convert node to string
func (n *node) String() string {
	return fmt.Sprintf("{%s->%v}", n.label, n.children)
}

// arc string reprenstation of single arc form n to n1
func (n *node) arc(n1 *node) string {
	return fmt.Sprintf("%s -> %s", n.label, n1.label)
}

// String convert node to string
func (n *node) arcs() []string {
	if len(n.children) > 0 {
		strArray := make([]string, 0, len(n.children))
		for _, n1 := range n.children {
			strArray = append(strArray, n.arc(n1))
		}
		return strArray
	}
	return nil
}

// child add child
func (n *node) child(n1 *node) {
	n1.parent = n
	n.children = append(n.children, n1)
}

// equals check if two nodes are same
func (n *node) equals(n1 *node) bool {
	return n.label == n1.label
}

// childIndex get the index of child
func (n *node) childIndex(n1 *node) int {
	for i := 0; i < len(n.children); i++ {
		if n.children[i].equals(n1) {
			return i
		}
	}
	return -1
}

// childRemove remove a node from children
func (n *node) childRemove(n1 *node) {
	if i := n.childIndex(n1); i != -1 {
		n.children = append(n.children[:i], n.children[i+1:]...)
	}
}

// reverse reverse parent child relationship
func (n *node) reverse() {
	// get parent
	if n1 := n.parent; n1 != nil {
		n1.childRemove(n)
		// first reverse the relationship
		n1.reverse()
		// add child
		n.child(n1)
	}
}

// AddNode add new node in graph
func (g *Graph) AddNode(nodeLabel string) {
	n := &node{label: nodeLabel}
	g.nodes[nodeLabel] = n
}

// AddArc add a node as a child
func (g *Graph) AddArc(from, to string) {
	n1, ok := g.nodes[from]
	// check if node exists
	if !ok {
		g.AddNode(from)
		n1 = g.nodes[from]
	}
	// Add relation ship
	n2 := g.nodes[to]
	n1.child(n2)
}

// ArcList get list representation of graph
func (g *Graph) ArcList() []string {
	s := make([]string, 0)
	for _, n := range g.nodes {
		s1 := n.arcs()
		s = append(s, s1...)
	}
	return s
}

// ChangeRoot change root of tree
func (g *Graph) ChangeRoot(oldRoot, newRoot string) *Graph {
	n1 := g.nodes[newRoot]
	n1.reverse()
	return g
}
