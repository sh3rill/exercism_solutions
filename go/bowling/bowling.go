package bowling

import (
	"errors"
	"fmt"
	"strconv"
)

// Frame Single Frame
type Frame struct {
	rolls [2]int
	score int
	id    int
}

// Game bowling game
type Game struct {
	throw   int
	current int
	end     bool
	frames  [10]*Frame
}

// NewFrame get new frame
func NewFrame(id int) *Frame {
	f := Frame{id: id, rolls: [2]int{-1, -1}}
	return &f
}

// NewGame get new game
func NewGame() *Game {
	frames := [10]*Frame{}
	for i := 0; i < len(frames); i++ {
		frames[i] = NewFrame(i + 1)
	}
	return &Game{frames: frames}
}

func (f *Frame) String() string {
	s := ""
	for i := 0; i < len(f.rolls); i++ {
		if f.rolls[i] == 10 {
			s += " X"
		} else if f.rolls[i] == 0 {
			s += " -"
		} else if f.rolls[i] > 0 {
			s += " " + strconv.Itoa(f.rolls[i])
		} else {
			s += " "
		}
	}
	return fmt.Sprintf("[%d>%d %s]", f.id, f.score, s)
}

func (g *Game) String() string {
	s := ""
	for i := 0; i <= g.current; i++ {
		f := g.Frame(i)
		if f != nil {
			s += g.frames[i].String()
		}
	}
	return fmt.Sprintf("{%s}", s)
}

// Strike check this frame is strike
func (f *Frame) Strike() bool {
	return f.rolls[0] == 10
}

// Spare check if this frame is spare
func (f *Frame) Spare() bool {
	return f.rolls[1]+f.rolls[0] >= 10
}

// Roll roll for this frame
func (f *Frame) Roll(pins int) bool {
	// Update score
	f.score += pins
	// Check if this is first throw of this frame
	if f.rolls[0] == -1 {
		f.rolls[0] = pins
		if pins == 10 && f.id < 10 {
			f.rolls[1] = 0
		}
		// check if this is valid pin
		return pins <= 10
	}
	// second throw
	f.rolls[1] = pins
	// Except for last frame each frame has maximum 10 pins
	if f.id < 10 || f.rolls[0] != 10 {
		return f.rolls[0]+f.rolls[1] <= 10
	}
	// Check if last frame does not exceed max ins
	return f.rolls[0]+f.rolls[1] <= 20
}

// Done check if frame is done
func (f *Frame) Done() bool {
	return (f.rolls[0] != -1 && f.rolls[1] != -1)
}

// Frame get frame at given index
func (g *Game) Frame(index int) *Frame {
	if index < 0 || index >= len(g.frames) {
		return nil
	}
	return g.frames[index]
}

// Last last frame
func (g *Game) Last() *Frame {
	return g.frames[len(g.frames)-1]
}

// Current current frame
func (g *Game) Current() *Frame {
	return g.Frame(g.current)
}

// Previous previous frame
func (g *Game) Previous() *Frame {
	return g.Frame(g.current - 1)
}

// NextFrame move to nextframe for game
func (g *Game) NextFrame() bool {
	if g.end {
		return true
	}
	// If we are running in bonus round
	if g.current == len(g.frames) {
		// Game is completed
		return true
	} else if g.current == len(g.frames)-1 {
		f := g.Current()
		if !f.Done() {
			return false
		}
		if f.Spare() || f.Strike() {
			g.current++
			return false
		}
		return true
	}
	if g.Current().Done() {
		g.current++
	}
	return false
}

// UpdatePreviousFrames update previous frames with score
func (g *Game) UpdatePreviousFrames(score int) {
	prev := g.Previous()
	if prev != nil {
		current := g.Current()
		if current == nil {
			// bonus round
			prev.score += score
		} else {
			// Check if this is first throw of frame
			if current.rolls[0] == -1 {
				// Check If last frame was a strike
				if prev.Strike() {
					prev.score += score
					f := g.Frame(g.current - 2)
					// Check if it was consecutive strikes
					if f != nil && f.Strike() {
						// Propgate score to one step previous frame
						f.score += score
					}
				} else if prev.Spare() {
					// It was a spare, propogate score to last frame
					prev.score += score
				}
			} else {
				// For second throw only last frame is updated
				if prev.Strike() {
					prev.score += score
				}
			}
		}
	}
}

// Roll roll update score for this roll
func (g *Game) Roll(pins int) error {
	g.throw++
	if pins < 0 || pins > 10 {
		return errors.New("Illegal value for pins")
	}
	if g.end {
		return errors.New("Game has eneded")
	}
	// Update privous frames score for any strike or spare
	g.UpdatePreviousFrames(pins)
	if g.current < len(g.frames) {
		// Update this frame score
		if !g.Current().Roll(pins) {
			// Invalid score
			return errors.New("A frame can not have more than 10 pins")
		}
	} else {
		// This is a bonus round
		// Check for valid score
		last := g.Last()
		if last.rolls[1] != 10 && last.rolls[1]+pins > 10 &&
			last.rolls[0]+last.rolls[1] != 10 {
			return errors.New("A frame can not have more than 10 pins")
		}
	}

	// Got to next frame and check if game has ended
	g.end = g.NextFrame()

	return nil
}

// Score return score of the game
func (g *Game) Score() (int, error) {
	if !g.end {
		return -1, errors.New("Game is still running")
	}
	score := 0
	for i := 0; i < len(g.frames); i++ {
		score += g.Frame(i).score
	}
	//fmt.Println(g, " : Score = ", score)
	return score, nil
}
