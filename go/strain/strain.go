package strain

// Ints collection of int
type Ints []int

// Lists collections of slices of int
type Lists [][]int

// Strings Collection of strings
type Strings []string

// Keep keep method for ints
func (i Ints) Keep(f func(int) bool) Ints {
	if len(i) == 0 {
		return nil
	}
	i1 := make(Ints, 0, len(i))
	for _, v := range i {
		if f(v) {
			i1 = append(i1, v)
		}
	}
	return i1
}

// Discard discard method for ints
func (i Ints) Discard(f func(int) bool) Ints {
	if len(i) == 0 {
		return nil
	}
	i1 := make(Ints, 0, len(i))
	for _, v := range i {
		if !f(v) {
			i1 = append(i1, v)
		}
	}
	return i1
}

// Keep keep method for lists
func (l Lists) Keep(f func([]int) bool) Lists {
	if len(l) == 0 {
		return nil
	}
	l1 := make(Lists, 0, len(l))
	for _, v := range l {
		if f(v) {
			l1 = append(l1, v)
		}
	}
	return l1
}

// Keep keep method for strings
func (s Strings) Keep(f func(string) bool) Strings {
	if len(s) == 0 {
		return nil
	}
	s1 := make(Strings, 0, len(s))
	for _, v := range s {
		if f(v) {
			s1 = append(s1, v)
		}
	}
	return s1
}
