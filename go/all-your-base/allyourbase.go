package allyourbase

import "errors"

func reverse(out []int) []int {
	for i, j := len(out)-1, 0; i > j; i, j = i-1, j+1 {
		out[i], out[j] = out[j], out[i]
	}
	return out
}

// BaseToDecimal convert base number to other
func BaseToDecimal(base int, digits []int) (int64, error) {
	var number int64
	var power int64 = 1
	if base < 2 {
		return -1, errors.New("input base must be >= 2")
	}
	for i := len(digits) - 1; i >= 0; i-- {
		n := int64(digits[i])
		if int(n) >= base || n < 0 {
			return -1, errors.New("all digits must satisfy 0 <= d < input base")
		}
		number += n * power
		power *= int64(base)
	}
	return number, nil
}

//DecimalToBase convert decimal number to base
func DecimalToBase(base int, number int64) ([]int, error) {
	out := make([]int, 0, 10)
	n := int64(base)
	if base < 2 {
		return nil, errors.New("output base must be >= 2")
	}
	//Handle negative number
	if number < 0 {
		number = -number
	}
	// Check if number if greater than 0
	if number != 0 {
		for number > 0 {
			m := int(number % n)
			out = append(out, m)
			number /= n
		}
	} else {
		// Should return atleast 0
		out = append(out, 0)
	}
	// Return of reverse of array
	return reverse(out), nil
}

// ConvertToBase convert a number from one base to another
func ConvertToBase(inBase int, digits []int, outBase int) ([]int, error) {
	// convert to decimal format
	n, err := BaseToDecimal(inBase, digits)
	if err != nil {
		return nil, err
	}
	return DecimalToBase(outBase, n)
}
