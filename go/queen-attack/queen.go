package queenattack

import (
	"errors"
)

// CanQueenAttack can queen q1 attack q2
func CanQueenAttack(q1, q2 string) (bool, error) {
	err := errors.New("Invalid pos")
	if len(q1) != 2 || len(q2) != 2 {
		return false, err
	}

	r1, c1 := q1[0]-'a'+1, q1[1]-'0'
	r2, c2 := q2[0]-'a'+1, q2[1]-'0'

	if r1 <= 0 || r1 >= 9 || c1 <= 0 || c1 >= 9 ||
		r2 <= 0 || r2 >= 9 || c2 <= 0 || c2 >= 9 {
		return false, err
	}

	if r1 == r2 {
		if c1 == c2 {
			return false, err
		}
		return true, nil
	}
	if c1 == c2 {
		return true, nil
	}
	if r1 < r2 {
		if c1 < c2 {
			if r2-r1 == c2-c1 {
				return true, nil
			}
			return false, nil
		}
		if r2-r1 == c1-c2 {
			return true, nil
		}
		return false, nil
	}
	if c1 < c2 {
		if r1-r2 == c2-c1 {
			return true, nil
		}
		return false, nil
	}
	if r1-r2 == c1-c2 {
		return true, nil
	}
	return false, nil
}
