package zebra

import "fmt"

type Color int
type Nationality int
type Pet int
type Smoke int
type Drink int
type Position int
type Step func()

const (
	NoColor Color = iota
	Red
	Green
	Yellow
	Blue
	Ivory
	LastColor = Ivory
)
const (
	NoNationality Nationality = iota
	Englishman
	Spaniard
	Ukrainian
	Norwegian
	Japanese
)
const (
	NoPet Pet = iota
	Dog
	Fox
	Zebra
	Horse
	Snails
)
const (
	NoDrink Drink = iota
	Tea
	Milk
	Water
	Coffee
	OrangeJuice
)

const (
	NoSmoke Smoke = iota
	Kools
	OldGold
	LuckyStrike
	Parliaments
	Chesterfields
)

const (
	NoPosition Position = iota
	First
	Second
	Third
	Fourth
	Fifth
	Middle = Third
	Last   = Fifth
)

var colors = [...]string{
	"None",
	"Red",
	"Green",
	"Yellow",
	"Blue",
	"Ivory",
}

var nationals = [...]string{
	"None",
	"Englishman",
	"Spaniard",
	"Ukrainian",
	"Norwegian",
	"Japanese",
}

var pets = [...]string{
	"None",
	"Dog",
	"Snails",
	"Fox",
	"Horse",
	"Zebra",
}

var drinks = [...]string{
	"None",
	"Coffee",
	"Tea",
	"Milk",
	"Orange Juice",
	"Water",
}

var smokes = [...]string{
	"None",
	"Old Gold",
	"Kools",
	"Chesterfields",
	"Lucky Strike",
	"Parliaments",
}
var positions = [...]string{
	"None",
	"First",
	"Second",
	"Third",
	"Fourth",
	"Fifth",
}
var steps = [...]Step{
	step2,
	step3,
	step4,
	step5,
	step6,
	step7,
	step8,
	step9,
	step10,
	step11,
	step12,
	step13,
	step14,
	step15,
}

func (c Color) String() string {
	return colors[c]
}
func (p Pet) String() string {
	return pets[p]
}
func (d Drink) String() string {
	return drinks[d]
}
func (s Smoke) String() string {
	return smokes[s]
}
func (n Nationality) String() string {
	return nationals[n]
}
func (p Position) String() string {
	return positions[p]
}

// House house for various params
type House struct {
	pet         Pet
	color       Color
	drink       Drink
	smoke       Smoke
	nationality Nationality
	position    Position
	next        *House
	prev        *House
}

type Solution struct {
	DrinksWater string
	OwnsZebra   string
}

var houses = [5]House{}
var houseColorMap = map[Color]*House{}
var houseSmokeMap = map[Smoke]*House{}
var housePetMap = map[Pet]*House{}
var houseDrinkMap = map[Drink]*House{}
var houseNationalityMap = map[Nationality]*House{}
var housePositionMap = map[Position]*House{}

func (h *House) SetPosition(position Position) {
	if housePositionMap[position] != nil {
		panic("Position Repated:" + position.String())
	}
	h.position = position
	housePositionMap[position] = h
	verifyPositions()
}

func (h *House) SetColor(color Color) {
	if houseColorMap[color] != nil {
		panic("Color Repated:" + color.String())
	}
	h.color = color
	houseColorMap[color] = h
	verifyColor()
}

func (h *House) SetPet(pet Pet) {
	if housePetMap[pet] != nil {
		panic("Pet Repated:" + pet.String())
	}
	h.pet = pet
	housePetMap[pet] = h
	verifyPet()
}

func (h *House) SetSmoke(smoke Smoke) {
	if houseSmokeMap[smoke] != nil {
		panic("Smoke Repated:" + smoke.String())
	}
	h.smoke = smoke
	houseSmokeMap[smoke] = h
	verifySmoke()
}

func (h *House) SetDrink(drink Drink) {
	if houseDrinkMap[drink] != nil {
		panic("Drink Repated:" + drink.String())
	}
	h.drink = drink
	houseDrinkMap[drink] = h
	verifyDrink()
}

func (h *House) SetNationality(nationality Nationality) {
	if houseNationalityMap[nationality] != nil {
		panic("Nationality Repated:" + nationality.String())
	}
	h.nationality = nationality
	houseNationalityMap[nationality] = h
	verifyNationality()
}

func (h House) String() string {
	return fmt.Sprintf("{Color:%v, Pet:%v, Drink:%v, Smoke:%v, Natoinality:%v, Position:%v}\n",
		h.color, h.pet, h.drink, h.smoke, h.nationality, h.position)
}

func getHouse(check func(*House) bool) *House {
	for i := 0; i < len(houses); i++ {
		h := &houses[i]
		if check(h) {
			return h
		}
	}
	return nil
}

func verifyNationality() {
	if len(houseNationalityMap) == 4 {
		n := NoNationality
		for i := 0; i < len(houses); i++ {
			n++
			if _, ok := houseNationalityMap[n]; !ok {
				h1 := getHouse(func(h *House) bool { return h.nationality == NoNationality })
				h1.SetNationality(n)
				return
			}
		}
	}
}

func verifyColor() {
	if len(houseColorMap) == 4 {
		n := NoColor
		for i := 0; i < len(houses); i++ {
			n++
			if _, ok := houseColorMap[n]; !ok {
				h1 := getHouse(func(h *House) bool {
					return h.color == NoColor
				})
				h1.SetColor(n)
				return
			}
		}
	}
}

func verifyPet() {
	if len(housePetMap) == 4 {
		n := NoPet
		for i := 0; i < len(houses); i++ {
			n++
			if _, ok := housePetMap[n]; !ok {
				h1 := getHouse(func(h *House) bool { return h.pet == NoPet })
				h1.SetPet(n)
				return
			}
		}
	}
}

func verifyDrink() {
	if len(houseDrinkMap) == 4 {
		n := NoDrink
		for i := 0; i < len(houses); i++ {
			n++
			if _, ok := houseDrinkMap[n]; !ok {
				h1 := getHouse(func(h *House) bool { return h.drink == NoDrink })
				h1.SetDrink(n)
				return
			}
		}
	}
}

func verifySmoke() {
	if len(housePetMap) == 4 {
		n := NoSmoke
		for i := 0; i < len(houses); i++ {
			n++
			if _, ok := houseSmokeMap[n]; !ok {
				h1 := getHouse(func(h *House) bool { return h.smoke == NoSmoke })
				h1.SetSmoke(n)
				return
			}
		}
	}
}

func verifyPositions() {
	if len(housePositionMap) == 4 {
		n := NoPosition
		for i := 0; i < len(houses); i++ {
			n++
			if _, ok := housePositionMap[n]; !ok {
				h1 := getHouse(func(h *House) bool { return h.position == NoPosition })
				h1.SetPosition(n)
				return
			}
		}
	} else {
		for p, h := range housePositionMap {
			if h.next != nil {
				p1 := p + 1
				if housePositionMap[p1] == nil {
					housePositionMap[p1] = h.next
				}
			} else if h.prev != nil {
				p1 := p - 1
				if housePositionMap[p1] == nil {
					housePositionMap[p1] = h.prev
				}
			}
		}
	}
}

func step2() {
	h1 := houseNationalityMap[Englishman]
	if h1 == nil {
		h1 = getHouse(func(h *House) bool { return h.nationality == NoNationality && (h.color == NoColor || h.color == Red) })
		h1.SetNationality(Englishman)
		verifyNationality()
	}
	h1.SetColor(Red)
	verifyColor()
}

func step3() {
	h1 := houseNationalityMap[Spaniard]
	if h1 == nil {
		h1 = getHouse(func(h *House) bool { return h.nationality == NoNationality && (h.pet == NoPet || h.pet == Dog) })
		h1.SetNationality(Spaniard)
		verifyNationality()
	}
	h1.SetPet(Dog)
	verifyPet()
}

func step4() {
	h1 := houseColorMap[Green]
	if h1 == nil {
		h1 = getHouse(func(h *House) bool { return h.color == NoColor && (h.drink == NoDrink || h.drink == Coffee) })
		h1.SetColor(Green)
		verifyColor()
	}
	h1.SetDrink(Coffee)
	verifyDrink()
}

func step5() {
	h1 := houseNationalityMap[Ukrainian]
	if h1 == nil {
		h1 = getHouse(func(h *House) bool { return h.nationality == NoNationality && (h.drink == NoDrink || h.drink == Tea) })
		h1.SetNationality(Ukrainian)
		verifyNationality()
	}
	h1.SetDrink(Tea)
	verifyDrink()
}

func step6() {
	h1 := houseColorMap[Green]
	if h1 == nil {
		h1 = getHouse(func(h *House) bool { return h.color == NoColor })
		h1.SetColor(Green)
		verifyColor()
	}
	h2 := houseColorMap[Ivory]
	if h2 == nil {
		h2 = getHouse(func(h *House) bool { return h.color == NoColor })
		h2.SetColor(Ivory)
		verifyColor()
	}
	h1.prev = h2
	h2.next = h1
	verifyPositions()
}

func step7() {
	h1 := houseSmokeMap[OldGold]
	if h1 == nil {
		h1 = getHouse(func(h *House) bool { return h.smoke == NoSmoke && (h.pet == NoPet || h.pet == Snails) })
		h1.SetSmoke(OldGold)
		verifySmoke()
	}
	h1.SetPet(Snails)
	verifyPet()
}

func step8() {
	h1 := houseColorMap[Yellow]
	if h1 == nil {
		h1 = getHouse(func(h *House) bool { return h.color == NoColor && (h.smoke == NoSmoke || h.smoke == Kools) })
		h1.SetColor(Yellow)
		verifyColor()
	}
	h1.SetSmoke(Kools)
}

func step9() {
	h1 := houseDrinkMap[Milk]
	if h1 == nil {
		h1 = getHouse(func(h *House) bool { return h.drink == NoDrink && (h.position == NoPosition || h.position == Middle) })
		h1.SetDrink(Milk)
		verifyDrink()
	}
	h1.SetPosition(Middle)
	verifyPositions()
}

func step10() {
	h1 := houseNationalityMap[Norwegian]
	if h1 == nil {
		h1 = getHouse(func(h *House) bool {
			return h.nationality == NoNationality && (h.position == NoPosition || h.position == First)
		})
		h1.SetNationality(Norwegian)
		verifyNationality()
	}
	h1.SetPosition(First)
	verifyPositions()
}

func step11() {
	h1 := housePetMap[Fox]
	if h1 == nil {
		h1 = getHouse(func(h *House) bool { return h.pet == NoPet })
		h1.SetPet(Fox)
		verifyPet()
	}
	h2 := houseSmokeMap[Chesterfields]
	if h2 == nil {
		h2 = getHouse(func(h *House) bool { return h.pet == NoPet })
		h2.SetSmoke(Chesterfields)
		verifySmoke()
	}
	h1.next = h2
	h2.prev = h1
	verifyPositions()
}

func step12() {
	h1 := housePetMap[Horse]
	if h1 == nil {
		h1 = getHouse(func(h *House) bool { return h.pet == NoPet })
		h1.SetPet(Horse)
		verifyPet()
	}
	h2 := houseSmokeMap[Kools]
	if h2 == nil {
		h2 = getHouse(func(h *House) bool { return h.smoke == NoSmoke })
		h2.SetSmoke(Kools)
		verifySmoke()
	}
	h1.next = h2
	h2.prev = h1
	verifyPositions()
}

func step13() {
	h1 := houseSmokeMap[LuckyStrike]
	if h1 == nil {
		h1 = getHouse(func(h *House) bool { return h.drink == NoDrink && h.smoke == NoSmoke })
		h1.SetSmoke(LuckyStrike)
		verifySmoke()
	}
	h1.SetDrink(OrangeJuice)
	verifyDrink()
}

func step14() {
	h1 := houseNationalityMap[Japanese]
	if h1 == nil {
		h1 = getHouse(func(h *House) bool {
			return h.nationality == NoNationality && (h.smoke == NoSmoke || h.smoke == Parliaments)
		})
		h1.SetNationality(Japanese)
		verifyNationality()
	}
	h1.SetSmoke(Parliaments)
	verifySmoke()
}

func step15() {
	h1 := houseNationalityMap[Norwegian]
	if h1 == nil {
		h1 = getHouse(func(h *House) bool { return h.nationality == NoNationality })
		h1.SetNationality(Norwegian)
		verifyNationality()
	}
	h2 := houseColorMap[Blue]
	if h2 == nil {
		h2 = getHouse(func(h *House) bool { return h.color == NoColor })
		h2.SetColor(Blue)
		verifyColor()
	}
	h2.next = h1
	h1.prev = h2
	verifyPositions()
}

func SolvePuzzle() Solution {
	/*1. There are five houses.
	2. The Englishman lives in the red house.
	3. The Spaniard owns the dog.
	4. Coffee is drunk in the green house.
	5. The Ukrainian drinks tea.
	6. The green house is immediately to the right of the ivory house.
	7. The Old Gold smoker owns snails.
	8. Kools are smoked in the yellow house.
	9. Milk is drunk in the middle house.
	10. The Norwegian lives in the first house.
	11. The man who smokes Chesterfields lives in the house next to the man with the fox.
	12. Kools are smoked in the house next to the house where the horse is kept.
	13. The Lucky Strike smoker drinks 	orange juice.
	14. The Japanese smokes Parliaments.
	15. The Norwegian lives next to the blue house.
	*/
	for _, step := range steps {
		step()
	}
	return Solution{DrinksWater: houseDrinkMap[Water].nationality.String(),
		OwnsZebra: housePetMap[Zebra].nationality.String()}
}
