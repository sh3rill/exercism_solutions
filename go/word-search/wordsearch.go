package wordsearch

import (
	"errors"
	"fmt"
)

// PuzzlePos Position in two dimensional puzzle plane
type PuzzlePos struct {
	x, y   int
	puzzle []string
}

// Direction
type Dirction int

const (
	Up Dirction = iota
	UpLeft
	Left
	DownLeft
	Down
	DownRight
	Right
	UpRight
)

func (p *PuzzlePos) Next(d Dirction) {
	switch d {
	case Up:
		p.y++
	case UpLeft:
		p.x, p.y = p.x-1, p.y+1
	case Left:
		p.x--
	case DownLeft:
		p.x, p.y = p.x-1, p.y-1
	case Down:
		p.y--
	case DownRight:
		p.x, p.y = p.x+1, p.y-1
	case Right:
		p.x++
	case UpRight:
		p.x, p.y = p.x+1, p.y+1
	}
}

func (p PuzzlePos) String() string {
	return fmt.Sprintf("{%d, %d}->{%c}", p.x, p.y, p.Char())
}

// Length get length of current string
func (p *PuzzlePos) Length() int {
	return len(p.puzzle[p.y])
}

// New Create Pozzle position marker
func New(x, y int, puzzle []string) *PuzzlePos {
	return &PuzzlePos{x, y, puzzle}
}

// Clone clone a marker
func (p *PuzzlePos) Clone() *PuzzlePos {
	return &PuzzlePos{p.x, p.y, p.puzzle}
}

// IsValid check if new marker is valid position
func (p *PuzzlePos) IsValid() bool {
	if p.x < 0 || p.y < 0 || p.y >= len(p.puzzle) || p.x >= len(p.puzzle[p.y]) {
		return false
	}
	return true
}

// Char get chracater at current marker
func (p *PuzzlePos) Char() byte {
	return p.puzzle[p.y][p.x]
}

// Match try to find a match in current position and direction
func (p *PuzzlePos) Match(d Dirction, word string) *PuzzlePos {
	l := len(word)
	i := 0
	// Make sure to clone
	pos := p.Clone()
	// Compare all characters of word, start with second chracter
	for i = 1; i < l; i++ {
		pos.Next(d)
		// Check if we cross the puzzle buondaries
		if !pos.IsValid() || word[i] != pos.Char() {
			return nil
		}
	}
	// Match
	if i < l {
		return nil
	}
	return pos
}

// Find word in puzzle
func Find(word string, puzzle []string) (*PuzzlePos, *PuzzlePos, error) {
	// Traverse all position in puzzle
	for p := New(0, 0, puzzle); p.IsValid(); p.Next(Up) {
		// Empty word in puzzle is not allowed
		if p.Length() == 0 {
			return nil, nil, errors.New("Empty String")
		}
		// Make Sure to cone the position marker
		for p1 := p.Clone(); p1.IsValid() && p1.Length() >= len(word); p1.Next(Right) {

			// Only go forward if first character match
			if word[0] == p1.Char() {

				// Try all dirsctions
				for d := Up; d <= UpRight; d++ {

					// If find a match return positions
					if p2 := p1.Match(d, word); p2 != nil {
						return p1, p2, nil
					}
				}
			}
		}
	}
	// No Match
	return nil, nil, errors.New("No Match")
}

// Solve solve puzzle
func Solve(words []string, puzzle []string) (map[string][2][2]int, error) {
	// Check for empty puzzle
	if len(words) == 0 || len(puzzle) == 0 {
		return nil, errors.New("Invalid Puzzle")
	}
	m := map[string][2][2]int{}

	// Travserse all words
	for _, word := range words {
		// Search this word in puzzle
		p1, p2, err := Find(word, puzzle)
		if err != nil {
			return nil, err
		}
		// Found a match
		m[word] = [2][2]int{{p1.x, p1.y}, {p2.x, p2.y}}
	}
	return m, nil
}
