package reverse

func String(s string) string {
	l := len(s)
	s1 := make([]byte, l, l)
	j := 0
	for i := l - 1; i >= 0; i-- {
		s1[j] = s[i]
		j++
	}
	return string(s1)
}
