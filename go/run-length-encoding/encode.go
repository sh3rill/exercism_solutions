package encode

import (
	"strconv"
	"unicode"
)

// RunLengthEncode run length encode a string
func RunLengthEncode(text string) string {
	var last rune = -1
	var lastPos int
	var str = ""
	for i, ch := range text {
		if i == 0 {
			last = ch
			lastPos = 0
		} else if last != ch {
			if i-lastPos > 1 {
				str += strconv.Itoa(i - lastPos)
			}
			str += string(last)
			lastPos = i
			last = ch
		}
	}
	if lastPos != len(text) {
		l := len(text)
		if l-lastPos > 1 {
			str += strconv.Itoa(l - lastPos)
		}
		str += string(last)
	}
	return str
}

func repeat(ch rune, count int) string {
	arr := make([]rune, count)
	for i := 0; i < len(arr); i++ {
		arr[i] = ch
	}
	return string(arr)
}

// RunLengthDecode run length decode a string
func RunLengthDecode(text string) string {
	var str = ""
	var count = 1
	arr := []rune(text)
	for i := 0; i < len(arr); i++ {
		ch := arr[i]
		if unicode.IsDigit(ch) {
			var j int
			count = int(ch - '0')
			for k, x := range arr[i+1:] {
				if !unicode.IsDigit(x) {
					j = k + i + 1
					break
				}
				count = count*10 + int(x-'0')
			}
			i = j - 1
		} else {
			s := repeat(ch, count)
			str += s
			count = 1
		}
	}
	return str
}
