package ledger

import (
	"errors"
	"fmt"
	"sort"
	"strings"
	"time"
)

// Entry ledger entry
type Entry struct {
	Date        string // "Y-m-d"
	Description string
	Change      int // in cents
}

// Entries slice for sort interface
type Entries []Entry

// Compare compare two entries
func (e Entry) Compare(e1 Entry) int {
	// First Compare Date
	if e.Date != e1.Date {
		if e.Date < e1.Date {
			return -4
		}
		return 4
	}
	// Compare Description
	if e.Description != e1.Description {
		if e.Description < e1.Description {
			return -2
		}
		return 2
	}
	// Compare Hcange
	if e.Change != e1.Change {
		if e.Change < e1.Change {
			return -1
		}
		return 1
	}
	return 0
}

// DateFormatter format date according locale
func DateFormatter(date, locale string) (string, error) {
	d, err := time.Parse("2006-01-02", date)
	if err != nil {
		return "", err
	}
	if locale == "nl-NL" {
		return d.Format("02-01-2006"), nil
	} else if locale == "en-US" {
		return d.Format("01/02/2006"), nil
	}
	return "", errors.New("")
}

func numberFormatter(n int, thousandSep, decimalSep string) string {
	str := fmt.Sprintf("%03d", n)
	s := ""
	decimal := str[len(str)-2:]
	rest := str[:len(str)-2]
	var parts []string
	for len(rest) > 3 {
		parts = append(parts, rest[len(rest)-3:])
		rest = rest[:len(rest)-3]
	}
	if len(rest) > 0 {
		parts = append(parts, rest)
	}
	for i := len(parts) - 1; i > 0; i-- {
		s += parts[i] + thousandSep
	}
	s += parts[0] + decimalSep + decimal
	return s
}

// CurrencyFormatter format currency according to locale
func CurrencyFormatter(cents int, currency, locale string) (string, error) {
	var a string
	negative := false
	if cents < 0 {
		cents = -cents
		negative = true
	}
	if locale == "nl-NL" {
		if currency == "EUR" {
			a += "€ "
		} else if currency == "USD" {
			a += "$ "
		}
		a += numberFormatter(cents, ".", ",")
		if negative {
			a += "-"
		} else {
			a += " "
		}
	} else if locale == "en-US" {
		if negative {
			a += "("
		}
		if currency == "EUR" {
			a += "€"
		} else if currency == "USD" {
			a += "$"
		}
		a += numberFormatter(cents, ",", ".")
		if negative {
			a += ")"
		} else {
			a += " "
		}
	}
	return a, nil
}

func formatString(str string, width int) string {
	l := len([]rune(str))
	if width == 0 {
		return str
	} else if width < 0 {
		width = -width
		if l > width {
			str = str[:width]
			l = width
		}
		return strings.Repeat(" ", width-l) + str
	}

	if l > width {
		str = str[:width]
		l = width
	}
	return str + strings.Repeat(" ", width-l)
}

// Format format entry with locale and currency
func (e Entry) Format(locale, currency string) (string, error) {

	d, err := DateFormatter(e.Date, locale)
	if err != nil {
		return "", err
	}
	a, err := CurrencyFormatter(e.Change, currency, locale)
	if err != nil {
		return "", err
	}

	de := ""
	if len(e.Description) > 25 {
		de = e.Description[:22] + "..."
	} else {
		de = e.Description
	}

	return formatString(d, 10) + " | " +
		formatString(de, 25) + " | " +
		formatString(a, -13) + "\n", nil
}

func (e Entries) Len() int {
	return len(e)
}
func (e Entries) Swap(i, j int) {
	e[i], e[j] = e[j], e[i]
}

func (e Entries) Less(i, j int) bool {
	x := e[i].Compare(e[j])
	return x < 0
}

// Format format all entries using locale and currency
func (e Entries) Format(locale, currency string) ([]string, error) {
	// Parallelism, always a great idea
	co := make(chan struct {
		i int
		s string
		e error
	})
	for i, et := range e {
		go func(i int, entry Entry) {
			s, err := entry.Format(locale, currency)
			co <- struct {
				i int
				s string
				e error
			}{i: i, s: s, e: err}
		}(i, et)
	}
	ss := make([]string, len(e))
	for range e {
		v := <-co
		if v.e != nil {
			return nil, v.e
		}
		ss[v.i] = v.s
	}

	return ss, nil
}

// Clone clone and create a copy of slice
func (e Entries) Clone() Entries {
	e1 := make(Entries, len(e))
	copy(e1, e)
	return e1
}

// Sort sort all entries
func (e Entries) Sort() {
	sort.Sort(e)
}

func header(locale string) string {
	if locale == "nl-NL" {
		return formatString("Datum", 10) + " | " + formatString("Omschrijving", 25) + " | Verandering\n"
	} else if locale == "en-US" {
		return formatString("Date", 10) + " | " + formatString("Description", 25) + " | Change\n"
	}
	return ""
}

// FormatLedger format ledger entrie
func FormatLedger(currency string, locale string, entries []Entry) (string, error) {
	// Check currency
	if currency != "EUR" && currency != "USD" {
		return "", errors.New("Unknown Currency")
	}
	s := header(locale)
	if s == "" {
		return "", errors.New("Unknown Locale")
	}
	if len(entries) > 0 {
		// copy and sort the list of entries
		e := Entries(entries).Clone()
		e.Sort()
		ss, err := e.Format(locale, currency)
		if err != nil {
			return "", err
		}
		for i := 0; i < len(ss); i++ {
			s += ss[i]
		}
	}
	fmt.Println(s)
	return s, nil
}
