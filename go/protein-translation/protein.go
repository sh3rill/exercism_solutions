package protein

var proteins = map[string]string{
	"AUG": "Methionine",
	"UUU": "Phenylalanine",
	"UUC": "Phenylalanine",
	"UUA": "Leucine",
	"UUG": "Leucine",
	"UCU": "Serine",
	"UCC": "Serine",
	"UCA": "Serine",
	"UCG": "Serine",
	"UAU": "Tyrosine",
	"UAC": "Tyrosine",
	"UGU": "Cysteine",
	"UGC": "Cysteine",
	"UGG": "Tryptophan",
	"UAA": "STOP",
	"UAG": "STOP",
	"UGA": "STOP",
}

func FromCodon(input string) string {
	return proteins[input]
}

func FromRNA(input string) []string {
	codes := make([]string, 0, len(input)/3)
	for i := 0; i <= len(input)-3; i += 3 {
		s := input[i : i+3]
		if c, ok := proteins[string(s)]; ok {
			if c == "STOP" {
				return codes
			}
			codes = append(codes, c)
		} else {
			println(" Not Found")
		}
	}

	return codes

}
