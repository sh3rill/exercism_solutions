package diamond

import (
	"errors"
	"fmt"
)

func row(r int, ch rune) string {
	if r < 0 {
		r = -r
	}
	if ch == 'A' {
		if r != 0 {
			return fmt.Sprintf("%*c%c%*c\n", r, ' ', ch, r, ' ')
		}
		return "A\n"
	}
	width := 2*(ch-'A') - 1
	if r != 0 {
		return fmt.Sprintf("%*c%c%*c%c%*c\n", r, ' ', ch, int(width), ' ', ch, r, ' ')
	}
	return fmt.Sprintf("%c%*c%c\n", ch, int(width), ' ', ch)
}

// Gen generate diamond
func Gen(end byte) (string, error) {
	if end >= 'A' && end <= 'Z' {
		str := ""
		r := int(end - 'A')
		ch := 'A'
		for i := -r; i <= r; i++ {
			s := row(i, ch)
			if i >= 0 {
				ch--
			} else {
				ch++
			}
			str += s
		}
		return str, nil

	}
	return "", errors.New("Invalid byte")

}
