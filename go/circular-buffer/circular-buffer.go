package circular

import (
	"errors"
	"fmt"
)

// ErrEmpty circular buffer is empty
var ErrEmpty = errors.New("Buffer is empty")

// ErrFull circular buffer is full
var ErrFull = errors.New("Buffer is full")

// Buffer circular buffer
type Buffer struct {
	data     []byte
	readPtr  int
	writePtr int
}

// NewBuffer create a new circular buffer
func NewBuffer(size int) *Buffer {
	d := make([]byte, size+1)
	return &Buffer{d, 0, 0}
}

// String convert circular buffer to string
func (b *Buffer) String() string {
	s := "["
	for i := 0; i < len(b.data); i++ {
		s += fmt.Sprintf("%c ", b.data[i])
	}
	s += "]"
	return fmt.Sprintf("{Read Ptr : %d, Write Ptr : %d, data :%s}", b.readPtr, b.writePtr, s)
}

// ReadByte read next byte from the circular buffer
func (b *Buffer) ReadByte() (byte, error) {
	var c byte
	if b.readPtr != b.writePtr {
		c = b.data[b.readPtr]
		b.readPtr = (b.readPtr + 1) % len(b.data)
		return c, nil
	}
	return c, ErrEmpty
}

// WriteByte write a byte to circular buffer
func (b *Buffer) WriteByte(c byte) error {
	nextPtr := (b.writePtr + 1) % len(b.data)
	if nextPtr != b.readPtr {
		b.data[b.writePtr] = c
		b.writePtr = nextPtr
		return nil
	}
	return ErrFull
}

// Overwrite force write byte in circular buffer
func (b *Buffer) Overwrite(c byte) {
	nextPtr := (b.writePtr + 1) % len(b.data)
	b.data[b.writePtr] = c
	b.writePtr = nextPtr
	if nextPtr == b.readPtr {
		nextPtr := (b.readPtr + 1) % len(b.data)
		b.readPtr = nextPtr
	}
}

// Reset circular buffer
func (b *Buffer) Reset() {
	b.readPtr = 0
	b.writePtr = 0
}
