package cipher

import (
	"strings"
	"unicode"
)

// Cipher cipher interface
type Cipher interface {
	Encode(string) string
	Decode(string) string
}

// Caesar caesar cipher
type Caesar struct {
	shift int
}

// Vigenere vigenere cipher
type Vigenere struct {
	secret string
}

// NewCaesar caesar cipher with default shift
func NewCaesar() Cipher {
	return &Caesar{3}
}

// NewShift caesar cipher with given shift
func NewShift(n int) Cipher {
	if n <= -26 || n >= 26 || n == 0 {
		return nil
	}
	return &Caesar{n}
}

// NewVigenere get vigenere cipher
func NewVigenere(key string) Cipher {
	// empty key not allowed
	if key == "" {
		return nil
	}
	alla := true
	// key should have all lower case alphabets
	for _, v := range key {
		if !unicode.IsLetter(v) || v < 'a' || v > 'z' {
			return nil
		}
		if v != 'a' {
			alla = false
		}
	}
	// all characters should not be a
	if alla {
		return nil
	}
	return &Vigenere{key}
}

// shift a single character
func shift(x, val int) byte {
	var y int
	// check case
	if x >= 'a' && x <= 'z' {
		y = x - 'a'
	} else if x >= 'A' && x <= 'Z' {
		y = x - 'A'
	}
	// shift and rotate
	y = (26 + y + val) % 26
	// encode only in lower case
	y += 'a'
	return byte(y)
}

// expand secret to length of plain text
func expand(plain, secret string) string {
	// number of times secret needs to be repeated
	delta := (len(plain) - len(secret)) / len(secret)
	// repeat secret
	newSecret := strings.Repeat(secret, delta+1)
	// extra character
	delta = len(plain) - len(newSecret)
	// append partial secret word
	if delta > 0 {
		newSecret += secret[:delta]
	}
	return newSecret
}

// Encode encode using caesar cipher
func (c *Caesar) Encode(plain string) string {
	b := make([]byte, len(plain))
	l := 0
	for _, v := range plain {
		// Only letters are allowed
		if unicode.IsLetter(v) {
			b[l] = shift(int(v), c.shift)
			l++
		}
	}
	return string(b[:l])
}

// Decode decode using caesar cipher
func (c *Caesar) Decode(cipher string) string {
	b := make([]byte, len(cipher))
	// decode cipher using negative of rotation
	for i, v := range cipher {
		b[i] = shift(int(v), -c.shift)
	}
	return string(b)
}

// Encode encode using vigenere
func (v *Vigenere) Encode(plain string) string {
	secret := v.secret
	// filter all non characters
	plain = strings.Join(strings.FieldsFunc(plain, func(r rune) bool {
		return !(unicode.IsLetter(r))
	}), "")
	// Secret should be same length that of plaintext
	if len(plain) > len(secret) {
		secret = expand(plain, secret)
	}
	b := make([]byte, len(plain))
	for i, v := range plain {
		u := int(v) - 'a'
		// Check if capital letters
		if v >= 'A' && v <= 'Z' {
			u = int(v) - 'A'
		}
		b[i] = shift(int(secret[i]), u)
	}
	return string(b)
}

// Decode decode using vigenere
func (v *Vigenere) Decode(cipher string) string {
	secret := v.secret
	if len(cipher) > len(secret) {
		// expand secret
		secret = expand(cipher, secret)
	}
	b := make([]byte, len(cipher))
	for i, v := range cipher {
		// decode
		b[i] = shift(int(v), -(int(secret[i]) - 'a'))
	}

	return string(b)
}
