package prime

var primes []uint64

func init() {
	primes = make([]uint64, 2, 1000)
	primes[0] = 2
	primes[1] = 3
}

// generate sieve
func sieve(limit uint64) []uint64 {
	last := primes[len(primes)-1]
	if limit <= last {
		return nil
	}
	numbers := make([]uint64, 0, (limit-last)/2+1)

OUTER:
	for i := 0; i < cap(numbers) && last <= limit-2; i++ {
		last += 2
		for _, p := range primes {
			if last%p == 0 {
				continue OUTER
			}
		}
		numbers = append(numbers, last)
	}
	return numbers
}

// Prime get primes using sieve
func Prime(limit uint64) []uint64 {
	if limit < 1 {
		return nil
	} else if limit == 1 {
		return []uint64{}
	}
	var arr []uint64
	numbers := sieve(limit)
	if numbers != nil {
		count := len(numbers)
		for i := 0; i < len(numbers); i++ {
			if numbers[i] != 0 {
				for j := i + 1; j < len(numbers); j++ {
					if numbers[j] != 0 && numbers[j]%numbers[i] == 0 {
						numbers[j] = 0
						count--
					}
				}
			}
		}

		arr = make([]uint64, len(primes), len(primes)+count)
		copy(arr, primes)
		for i := 0; i < len(numbers); i++ {
			if numbers[i] != 0 {
				arr = append(arr, numbers[i])
				primes = append(primes, numbers[i])
			}
		}
	} else {
		var i int
		var n uint64
		for i, n = range primes {
			if limit < n {
				break
			}
		}
		arr = make([]uint64, i)
		copy(arr, primes[:i])
	}
	return arr
}

// Nth get nth prime number
func Nth(n int) (int, bool) {
	if n <= 0 {
		return -1, false
	}

	for i := uint64(n * 2); len(primes) < n; i <<= 1 {
		_ = Prime(i)
	}

	return int(primes[n-1]), true
}
