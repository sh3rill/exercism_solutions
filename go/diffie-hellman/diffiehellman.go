package diffiehellman

import (
	"crypto/rand"
	"math/big"
)

var one = big.NewInt(1)

// PrivateKey generate private key a, b, make sure it is greater than 1
func PrivateKey(p *big.Int) *big.Int {
	var n *big.Int
	for {
		n, _ = rand.Int(rand.Reader, p)
		if n.Cmp(one) > 0 {
			break
		}
	}
	return n
}

// PublicKey generate public key using private key A = g**a mod p, B = g**b mod p
func PublicKey(private, p *big.Int, g int64) *big.Int {
	gBig := big.NewInt(g)
	n := new(big.Int)
	n.Exp(gBig, private, p)
	return n
}

// NewPair create public and private key pair
func NewPair(p *big.Int, g int64) (private, public *big.Int) {
	n := PrivateKey(p)
	return n, PublicKey(n, p, g)
}

// SecretKey create secret key using own private key and others public key, s = B**a mod p, s = A**b mod p
func SecretKey(private1, public2, p *big.Int) *big.Int {
	n := new(big.Int)
	n.Exp(public2, private1, p)
	return n
}
