package kindergarten

import (
	"errors"
	"sort"
	"strings"
)

// Garden garden
type Garden struct {
	children []string
	plants   map[string][4]byte
}

// PlantName plant names
var PlantName = map[byte]string{
	'G': "grass", 'C': "clover", 'R': "radishes", 'V': "violets",
}

// NewGarden get new garden from children strings and diagram
func NewGarden(diagram string, children []string) (*Garden, error) {
	childrenCopy := make([]string, len(children))
	copy(childrenCopy, children)
	sort.Slice(childrenCopy, func(i, j int) bool { return childrenCopy[i] < childrenCopy[j] })
	sArr := strings.Split(diagram, "\n")
	if len(sArr) != 3 {
		return nil, errors.New("digram can not be more than two lines")
	}
	sArr = sArr[1:]
	if len(sArr[0]) != 2*len(children) || len(sArr[0]) != len(sArr[1]) {
		return nil, errors.New("Diagram length mismatch")
	}
	g := Garden{children: children, plants: map[string][4]byte{}}
	for i, child := range childrenCopy {
		j := i * 2
		if _, ok := g.plants[child]; ok {
			return nil, errors.New("duplicate names")
		}
		_, ok1 := PlantName[sArr[0][j]]
		_, ok2 := PlantName[sArr[0][j+1]]
		_, ok3 := PlantName[sArr[1][j]]
		_, ok4 := PlantName[sArr[1][j+1]]
		if !ok1 || !ok2 || !ok3 || !ok4 {
			return nil, errors.New("invalid plant names")
		}
		g.plants[child] = [4]byte{sArr[0][j], sArr[0][j+1], sArr[1][j], sArr[1][j+1]}
	}
	return &g, nil
}

// Plants get plants for a child
func (g *Garden) Plants(child string) ([]string, bool) {
	if p, ok := g.plants[child]; ok {
		return []string{PlantName[p[0]], PlantName[p[1]], PlantName[p[2]], PlantName[p[3]]}, true
	}
	return nil, false
}
