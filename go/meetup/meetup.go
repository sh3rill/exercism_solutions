package meetup

import "time"

type WeekSchedule int

const (
	First WeekSchedule = iota
	Second
	Third
	Fourth
	Last
	Teenth
)

// Day get day of month
func Day(sch WeekSchedule, weekday time.Weekday, month time.Month, year int) int {
	var t time.Time
	if sch >= First && sch <= Fourth {
		// first, seconds, third and fourth weeks, get to first week and then multiple of 7
		t = time.Date(year, month, 01, 00, 00, 00, 00, time.Local)
		duration := int(weekday - t.Weekday())
		if duration < 0 {
			duration = 7 + duration
		}
		duration = duration + int(sch)*7
		if duration >= 0 {
			t = t.AddDate(0, 0, duration)
		}
	} else if sch == Teenth {
		// Days 13 - 19
		t = time.Date(year, month, 13, 00, 00, 00, 00, time.Local)
		duration := int(weekday - t.Weekday())
		if duration < 0 {
			duration = 7 + duration
		}
		if duration >= 0 {
			t = t.AddDate(0, 0, duration)
		}
	} else if sch == Last {
		// Get to Last week and add week days
		t = time.Date(year, month+1, 01, 00, 00, 00, 00, time.Local)
		t = t.AddDate(0, 0, -7)
		duration := int(weekday - t.Weekday())
		if duration < 0 {
			duration = 7 + duration
		}
		if duration >= 0 {
			t = t.AddDate(0, 0, duration)
		}
	}
	return t.Day()
}
