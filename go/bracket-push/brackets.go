package brackets

func parse(data []rune, end rune) bool {
	for i, val := range data {
		switch val {
		case end:
			return true
		case '[':
			return parse(data[i+1:], ']')
		case '(':
			return parse(data[i+1:], ')')
		case '{':
			return parse(data[i+1:], '}')
		case '}':
			return false
		case ']':
			return false
		case ')':
			return false
		default:
			continue
		}
	}

	if end == ' ' {
		return true
	}
	return false
}

func Bracket(input string) (bool, error) {
	return parse([]rune(input), ' '), nil
}
