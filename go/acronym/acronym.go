// Package acronym should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package acronym

import (
	"strings"
	"unicode"
)

// Abbreviate should have a comment documenting it.
func Abbreviate(s string) string {
	arr := strings.FieldsFunc(s, func(c rune) bool {
		return !unicode.IsLetter(c)
	})
	s1 := make([]byte, len(arr))
	for i, elem := range arr {
		s1[i] = elem[0]
		if s1[i] >= 'a' && s1[i] <= 'z' {
			s1[i] -= 'a'
			s1[i] += 'A'
		}
	}
	return string(s1)
}
