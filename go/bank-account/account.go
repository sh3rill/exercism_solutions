package account

import "sync"

// Account bank acount object
type Account struct {
	amount int
	closed bool
	mutex  *sync.Mutex
}

// Open open a new account
func Open(amount int) *Account {
	if amount >= 0 {
		m := &sync.Mutex{}
		return &Account{amount, false, m}
	}
	return nil
}

// Balance get balance of account
func (a *Account) Balance() (int, bool) {
	a.mutex.Lock()
	defer a.mutex.Unlock()
	if !a.closed {
		return a.amount, true
	}
	return 0, false
}

// Deposit desposit or withdraw amount in account
func (a *Account) Deposit(amt int) (int, bool) {
	a.mutex.Lock()
	defer a.mutex.Unlock()
	if !a.closed && a.amount+amt >= 0 {
		a.amount += amt
		return a.amount, true
	}
	return 0, false
}

// Close close account
func (a *Account) Close() (int, bool) {
	a.mutex.Lock()
	defer a.mutex.Unlock()
	if !a.closed {
		amt := a.amount
		a.amount = 0
		a.closed = true
		return amt, true
	}
	return 0, false
}
