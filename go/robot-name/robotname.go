package robotname

import (
	"fmt"
	"math/rand"
	"time"
)

// Robot robot structure
type Robot struct {
	name string
}

var names = map[string]interface{}{}
var count int

const numMax = 26 * 26 * 1000

func init() {
	rand.Seed(time.Now().Unix())
}

func numberToString(n int) string {
	x := n % 1000
	n /= 1000
	i := n/26 + 'A'
	j := n%26 + 'A'
	return fmt.Sprintf("%c%c%03d", i, j, x)
}

func gen() string {
	return numberToString(rand.Intn(numMax))
}

func newName() string {
	if count < numMax {
		for i := 0; i < numMax; i++ {
			s := gen()
			if _, ok := names[s]; !ok {
				names[s] = true
				count++
				return s
			}
		}
		for i := 0; i < numMax; i++ {
			s := numberToString(i)
			if _, ok := names[s]; !ok {
				names[s] = true
				count++
				return s
			}
		}
	}
	fmt.Println("All posiible values have been generated", count, numMax)
	return ""
}

// Name get name of robot
func (r *Robot) Name() string {
	if r.name == "" {
		r.name = newName()
	}
	return r.name
}

// Reset reset robot's name
func (r *Robot) Reset() {
	r.name = newName()
}
