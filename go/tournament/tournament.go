package tournament

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"sort"
	"strings"
)

type Team struct {
	name                              string
	wins, loss, draw, points, matches int
}

type Result struct {
	scanner *bufio.Scanner
	order   []int
	ids     map[string]int
	teams   []*Team
}

func (t1 *Team) Compare(t2 *Team) bool {
	if t1.points == t2.points {
		if t1.wins == t2.wins {
			if t1.loss == t2.loss {
				if t1.draw == t2.draw {
					if t1.matches == t2.matches {
						return t1.name > t2.name
					}
					return t1.matches < t2.matches
				}
				return t1.draw < t2.draw

			}
			return t1.loss < t2.loss
		}
		return t1.wins < t2.wins
	}
	return t1.points < t2.points
}

func (r *Team) String() string {
	return fmt.Sprintf("%-31s|  %-2d|  %-2d|  %-2d|  %-2d|  %d",
		r.name, r.matches, r.wins, r.draw, r.loss, r.points)
}

func (t *Team) Win() {
	t.wins++
	t.matches++
	t.points += 3
}

func (t *Team) Loss() {
	t.loss++
	t.matches++
}

func (t *Team) Draw() {
	t.draw++
	t.matches++
	t.points++
}

func (r *Result) TeamId(name string) int {
	if id, ok := r.ids[name]; ok {
		return id
	}
	team := Team{name, 0, 0, 0, 0, 0}
	id := len(r.teams)
	r.teams = append(r.teams, &team)
	r.ids[name] = id
	return id
}

func (r *Result) Team(name string) *Team {
	id := r.TeamId(name)
	return r.teams[id]
}

func (r *Result) Match(a, b, result string) error {
	teamA := r.Team(a)
	teamB := r.Team(b)
	switch result {
	case "win":
		teamA.Win()
		teamB.Loss()
	case "loss":
		teamA.Loss()
		teamB.Win()
	case "draw":
		teamA.Draw()
		teamB.Draw()
	default:
		return errors.New("Unknown result")
	}
	return nil
}

func (r *Result) Scan() error {
	var a, b, result string
	for {
		if a = r.Read(); a == "" {
			break
		}

		if b = r.Read(); b == "" {
			return errors.New("Format Error team B")
		}

		if result = r.Read(); result == "" {
			return errors.New("Format Error team result")
		}
		err := r.Match(a, b, result)
		if err != nil {
			return err
		}
	}
	return nil
}

func (r *Result) Header() string {
	return fmt.Sprint("Team                           | MP |  W |  D |  L |  P")
}

func (r *Result) Finish() {
	r.order = make([]int, len(r.teams))
	for i := range r.order {
		r.order[i] = i
	}
	sort.Slice(r.order, func(i, j int) bool {
		return r.teams[r.order[j]].Compare(r.teams[r.order[i]])
	})
}

func newResults(input io.Reader) *Result {
	scanner := bufio.NewScanner(input)
	t := make([]*Team, 0, 100)
	m := make(map[string]int)
	r := Result{scanner, nil, m, t}

	onSemicolon := func(data []byte, atEOF bool) (advance int, token []byte, err error) {
		if atEOF && len(data) == 0 {
			return 0, nil, nil
		}
		if data[0] == '#' {
			for i := 1; i < len(data); i++ {
				if data[i] == '\n' || data[i] == '\r' {
					return i + 1, data[:i], nil
				}
			}
			return 0, data, bufio.ErrFinalToken
		}
		for i := 0; i < len(data); i++ {
			if data[i] == '#' || data[i] == ';' || data[i] == '\n' || data[i] == '\r' {
				return i + 1, data[:i], nil
			}
		}
		return 0, data, bufio.ErrFinalToken
	}
	r.scanner.Split(onSemicolon)
	return &r
}

func (r *Result) Read() string {
	var s string
	for r.scanner.Scan() {
		s = r.scanner.Text()
		if s != "" && s[0] != '#' {
			break
		}
	}
	return strings.TrimSpace(s)
}

func Tally(input io.Reader, out io.Writer) error {
	r := newResults(input)
	if err := r.Scan(); err != nil {
		return err
	}
	r.Finish()
	fmt.Fprintln(out, r.Header())
	for _, id := range r.order {
		fmt.Fprintln(out, r.teams[id])
	}

	return nil
}
