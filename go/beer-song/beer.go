package beer

import (
	"errors"
	"fmt"
)

// Bottle count of bottels
type Bottle struct {
	count int
}

// Conert bottel to string
func (b *Bottle) String() string {
	if b.count == 0 {
		return "no more bottles"
	} else if b.count == 1 {
		return "1 bottle"
	}
	return fmt.Sprintf("%d bottles", b.count)
}

// Use use one bottle
func (b *Bottle) Use() string {
	if b.count == 0 {
		return "Go to the store and buy some more"
	} else if b.count == 1 {
		return "Take it down and pass it around"
	}
	return "Take one down and pass it around"
}

// TakeOne get one bottel,
func (b *Bottle) TakeOne() {
	if b.count == 0 {
		b.count = 99
	} else {
		b.count--
	}
}

// Verse return nth verse of songs
func Verse(n int) (string, error) {
	if n >= 0 && n <= 99 {
		b := &Bottle{n}
		s := ""
		if n == 0 {
			s = "No more bottles of beer on the wall"
		} else {
			s = fmt.Sprintf("%s of beer on the wall", b)
		}

		s += fmt.Sprintf(", %s of beer.\n%s", b, b.Use())
		b.TakeOne()
		s += fmt.Sprintf(", %s of beer on the wall.\n", b)
		return s, nil
	}
	return "", errors.New("Out of range")
}

// Verses return verses of song
func Verses(start, end int) (string, error) {
	if start < end {
		return "", errors.New("Out of range")
	}
	str := ""
	for i := start; i >= end; i-- {
		s, err := Verse(i)
		if err != nil {
			return "", err
		}
		str += s + "\n"
	}
	return str, nil
}

// Song sing all song
func Song() string {
	str, _ := Verses(99, 0)
	return str
}
