package cryptosquare

import (
	"math"
	"strings"
)

func normalize(str string) []rune {
	n := make([]rune, 0, len(str))
	for _, val := range str {
		if val >= 'A' && val <= 'Z' {
			val -= 'A'
			val += 'a'
		}
		if (val >= 'a' && val <= 'z') ||
			(val >= '0' && val <= '9') {
			n = append(n, val)
		}

	}
	return n
}

func Encode(plainText string) string {
	normalized := normalize(plainText)
	l := int(math.Ceil(math.Sqrt(float64(len(normalized)))))
	crypt := make([]string, l)
	for i := 0; i < l; i++ {
		for j := i; j < len(normalized); j += l {
			crypt[i] += string(normalized[j])
		}
	}

	return strings.Join(crypt, " ")
}
