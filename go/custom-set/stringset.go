package stringset

import "fmt"

// Set set of string
type Set struct {
	data map[string]struct{}
}

// New get a new set
func New() Set {
	return Set{data: make(map[string]struct{})}
}

// NewFromSlice convert slice of strings to Set
func NewFromSlice(args []string) Set {
	s := New()
	for _, str := range args {
		s.data[str] = struct{}{}
	}
	return s
}

// Clone clone a set and create a new one
func Clone(s Set) Set {
	s1 := New()
	for key := range s.data {
		s1.Add(key)
	}
	return s1
}

//Length get the count of elements of set
func (s Set) Length() int {
	return len(s.data)
}

// Elements get the elements of set
func (s Set) Elements() []string {
	strs := make([]string, s.Length())
	i := 0
	for key := range s.data {
		strs[i] = key
	}
	return strs
}

// IsEmpty check if set is empty
func (s Set) IsEmpty() bool {
	return len(s.data) == 0
}

// Add add a string in ti a new set
func (s Set) Add(v string) {
	s.data[v] = struct{}{}
}

// Has check if set has a key
func (s Set) Has(str string) bool {
	_, ok := s.data[str]
	return ok
}

// String convert set to string
func (s Set) String() string {
	str := "{"
	i := 0
	for key := range s.data {
		if i == 0 {
			str += fmt.Sprintf("\"%s\"", key)
		} else {
			str += fmt.Sprintf(", \"%s\"", key)
		}
		i++
	}
	str += "}"
	return str
}

// Copy copy one set in to other
func (s Set) Copy(s1 Set) {
	for key := range s1.data {
		s.data[key] = struct{}{}
	}
}

// Subset check if s2 is subset of s1
func Subset(s1, s2 Set) bool {
	for key := range s1.data {
		if !s2.Has(key) {
			return false
		}
	}
	return true
}

// Disjoint check if two sets s1 and s2 are disjoint
func Disjoint(s1, s2 Set) bool {
	for key := range s1.data {
		if s2.Has(key) {
			return false
		}
	}
	for key := range s2.data {
		if s1.Has(key) {
			return false
		}
	}
	return true
}

// Equal check if two sets are equal
func Equal(s1, s2 Set) bool {
	return len(s1.data) == len(s2.data) && Subset(s1, s2)
}

// Union get union of two sets
func Union(s1, s2 Set) Set {
	s := Clone(s1)
	s.Copy(s2)
	return s
}

// Intersection get the insection of two sets s1 and s2
func Intersection(s1, s2 Set) Set {
	s := New()
	for key := range s1.data {
		if s2.Has(key) {
			s.Add(key)
		}
	}
	return s
}

// Difference get the relative complement of s1 in s2
func Difference(s1, s2 Set) Set {
	s := New()
	for key := range s1.data {
		if !s2.Has(key) {
			s.Add(key)
		}
	}
	return s
}
