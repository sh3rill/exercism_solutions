package sublist

type Relation string

func match(a, b []int) int {
	if len(a) < len(b) {
		return -1
	}
	for i := 0; i < len(b); i++ {
		if a[i] != b[i] {
			return i
		}
	}
	return len(b)
}

func Sublist(a, b []int) Relation {
	if len(a) > len(b) {
		m := 0
		for {
			m = match(a, b)
			if m >= len(b) {
				return "superlist"
			}
			if m < 0 {
				return "unequal"
			}
			a = a[1:]
			if len(a) < len(b) {
				return "unequal"
			}
		}
	}
	if len(a) < len(b) {
		m := 0
		for {
			m = match(b, a)
			if m >= len(a) {
				return "sublist"
			}
			if m < 0 {
				return "unequal"
			}
			b = b[1:]
			if len(b) < len(a) {
				return "unequal"
			}
		}
	}
	for i, elem := range a {
		if b[i] != elem {
			return "unequal"
		}
	}

	return "equal"
}
