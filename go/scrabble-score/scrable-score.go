package scrabble

var m = map[byte]int{
	'D': 2,
	'G': 2,
	'B': 3,
	'C': 3,
	'M': 3,
	'P': 3,
	'F': 4,
	'H': 4,
	'V': 4,
	'W': 4,
	'Y': 4,
	'K': 5,
	'J': 8,
	'X': 8,
	'Q': 10,
	'Z': 10,
}

func Score(s string) int {
	l := len(s)
	var n int
	for i := 0; i < l; i++ {
		ch := s[i]
		if ch >= 'a' && ch <= 'z' {
			ch = ch - 'a' + 'A'
		}
		if m, ok := m[ch]; ok {
			n += m
		} else {
			n++
		}
	}
	return n
}
