package series

func UnsafeFirst(n int, s string) string {
	if n <= len(s) {
		return s[:n]
	}
	return ""
}

func First(n int, s string) (string, bool) {
	if n <= len(s) {
		return s[:n], true
	}
	return "", false
}
func All(n int, s string) []string {
	var l []string
	if n <= len(s) {
		l = make([]string, 0, len(s)-n+1)
		for i := 0; i <= len(s)-n; i++ {
			l = append(l, s[i:n+i])
		}
	} else {
		return nil
	}
	return l
}
