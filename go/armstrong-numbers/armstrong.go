package armstrong

import "strconv"
import "math"

// IsNumber check if number is armstrong number
func IsNumber(number int) bool {
	str := strconv.Itoa(number)
	l := len(str)
	x := 0
	for _, n := range str {
		x += int(math.Pow(float64(n-'0'), float64(l)))
	}
	return x == number
}
