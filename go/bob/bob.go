// Package bob should have a package comment that summarizes what it's about.
// https://golang.org/doc/effective_go.html#commentary
package bob

// Hey should have a comment documenting it.
func Hey(remark string) string {

	l := len(remark)
	if l <= 0 {
		return "Fine. Be that way!"
	}

	shout, que, empty := false, false, true
	if remark[l-1] == '?' {
		que = true
	}
LOOP:
	for i := l - 1; i >= 0; i-- {
		ch := remark[i]
		switch {
		case ch == '?':
			if empty {
				que = true
			}
		case ch >= 'a' && ch <= 'z':
			shout = false
			empty = false
			break LOOP
		case ch >= 'A' && ch <= 'Z':
			shout = true
			empty = false
		case ch == ' ':
		case ch == '\t':
		case ch == '\r':
		case ch == '\n':
		default:
			empty = false
		}
	}

	if que {
		if shout {
			return "Calm down, I know what I'm doing!"
		}
		return "Sure."
	}

	if shout {
		return "Whoa, chill out!"
	}

	if empty {
		return "Fine. Be that way!"
	}

	return "Whatever."
}
