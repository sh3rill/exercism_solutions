package igpay

import "strings"

// isVowel check if character is vowel
func isVowel(ch byte) bool {
	if ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' ||
		ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U' {
		return true
	}
	return false
}

// translate translate single word to piglatin
func translate(in string) string {
	out := ""
	for i, s := range in {
		if isVowel(byte(s)) && (i < 1 || (s != 'u' || in[i-1] != 'q')) {
			out = in[i:] + in[:i]
			break
		} else if (s == 'y' || s == 'x') && (i < len(in)-1 && !isVowel(in[i+1])) {
			out = in[i:] + in[:i]
			break
		}
	}
	return out + "ay"
}

// PigLatin piglatin sentences
func PigLatin(in string) string {
	parts := strings.Split(in, " ")
	outParts := make([]string, len(parts))
	for i, part := range parts {
		outParts[i] = translate(part)
	}
	return strings.Join(outParts, " ")
}
