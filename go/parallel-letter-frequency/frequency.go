package letter

import (
	"sync"
)

//FreqMap a map of frequency of all chracters
type FreqMap map[rune]int

// Frequency calculate frequency of all char
func Frequency(s string) FreqMap {
	m := FreqMap{}
	for _, r := range s {
		m[r]++
	}
	return m
}

func traverse(s string) <-chan rune {
	t := make(chan rune)
	go func() {
		for _, r := range s {
			t <- r
		}
		close(t)
	}()
	return t
}

func merge(cs []<-chan rune) <-chan rune {
	var wg sync.WaitGroup
	out := make(chan rune)

	output := func(c <-chan rune) {
		for n := range c {
			out <- n
		}
		wg.Done()
	}
	wg.Add(len(cs))
	for _, c := range cs {
		go output(c)
	}

	go func() {
		wg.Wait()
		close(out)
	}()
	return out
}

//ConcurrentFrequency Calculate concurrent frequency count on all strings
func ConcurrentFrequency(arr []string) FreqMap {
	l := len(arr)
	m := FreqMap{}
	chans := make([]<-chan rune, l, l)
	for i := 0; i < l; i++ {
		chans[i] = traverse(arr[i])
	}
	for n := range merge(chans) {
		m[n]++
	}
	return m
}
