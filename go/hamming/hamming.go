package hamming

import "errors"

func Distance(a, b string) (int, error) {
	l1 := len(a)
	l2 := len(b)
	if l1 == l2 {
		d := 0
		for i := 0; i < l1; i++ {
			if a[i] != b[i] {
				d++
			}
		}
		return d, nil
	}

	return -1, errors.New("Length mismatch")
}
