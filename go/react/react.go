package react

import (
	"container/list"
	"fmt"
)

const (
	CellTypeNull = iota + 1
	CellTypeInput
	CellTypeCompute
)

type cellInput func([]*cell) int

type cell struct {
	id        int
	value     int
	newValue  int
	lastValue int
	cellType  int
	function  cellInput
	callbacks *list.List
	inputs    []*cell
	observer  *list.List
	sheet     *sheet
}

type canceller struct {
	cell *cell
	elem *list.Element
}

type sheet struct {
	newid     int
	cellList  *list.List
	dirtyList *list.List
}

func (c *cell) String() string {
	return fmt.Sprintf("{id:%d, new:%d, val:%d, type:%d}", c.id, c.newValue, c.value, c.cellType)
}

func (c *cell) Value() int {
	return c.value
}

func (c *cell) AddCallback(f func(int)) Canceler {
	e := c.callbacks.PushBack(f)
	return &canceller{cell: c, elem: e}
}

func (c *canceller) Cancel() {
	if c.cell != nil {
		c.cell.callbacks.Remove(c.elem)
		c.cell = nil
		c.elem = nil
	}
}

func (c *cell) SetValue(v int) {
	c.newValue = v
	if c.save() {
		c.dirty()
		c.notify()
		c.sheet.trigger()
	}
}

func (c *cell) dirty() {
	c.sheet.dirtyCell(c)
}

func (c *cell) save() bool {
	if c.value == c.newValue {
		return false
	}
	c.lastValue = c.value
	c.value = c.newValue
	return true
}

func (c *cell) update() bool {
	if c.function == nil {
		return false
	}
	c.newValue = c.function(c.inputs)
	return c.newValue != c.value
}

func (c *cell) triggerCallbacks() {
	for e := c.callbacks.Front(); e != nil; e = e.Next() {
		f := e.Value.(func(int))
		f(c.value)
	}
}

func (c *cell) notify() {
	//fmt.Println("notify", c)
	for e := c.observer.Front(); e != nil; e = e.Next() {
		value := e.Value.(*cell)
		if value.update() {
			value.dirty()
			value.notify()
		}
	}
}

func (s *sheet) dirtyCell(c *cell) {
	s.dirtyList.PushBack(c)
}

func (s *sheet) trigger() {
	for e := s.dirtyList.Front(); e != nil; e = e.Next() {
		c := e.Value.(*cell)
		if c.save() {
			//fmt.Println(c)
			c.triggerCallbacks()
		}
	}
	s.dirtyList.Init()
}

func (s *sheet) newCell(cellType int, f cellInput, inputs []*cell) *cell {
	val := 0
	if f != nil {
		val = f(inputs)
	}
	c := &cell{
		cellType:  cellType,
		value:     val,
		newValue:  val,
		function:  f,
		inputs:    inputs,
		callbacks: list.New(),
		observer:  list.New(),
		sheet:     s,
	}
	for _, input := range inputs {
		input.observer.PushBack(c)
	}
	s.cellList.PushBack(c)
	c.id = s.newid
	s.newid++
	return c
}

func (s *sheet) CreateInput(v int) InputCell {
	c := s.newCell(CellTypeInput, nil, nil)
	c.value = v
	c.newValue = v
	return c
}

func (s *sheet) CreateCompute1(c1 Cell, f func(int) int) ComputeCell {
	c := c1.(*cell)
	wrapper := func(args []*cell) int {
		return f(args[0].newValue)
	}
	return s.newCell(CellTypeCompute, wrapper, []*cell{c})
}

func (s *sheet) CreateCompute2(c1 Cell, c2 Cell, f func(int, int) int) ComputeCell {
	c3 := c1.(*cell)
	c4 := c2.(*cell)
	wrapper := func(args []*cell) int {
		return f(args[0].newValue, args[1].newValue)
	}
	return s.newCell(CellTypeCompute, wrapper, []*cell{c3, c4})
}

func New() Reactor {
	return &sheet{
		dirtyList: list.New(),
		cellList:  list.New(),
	}
}
