package isbn

import "unicode"

// IsValidISBN check for valid ISBN number
func IsValidISBN(number string) bool {
	check := 0
	coeff := 10
	for _, ch := range number {
		if coeff == 0 {
			return false
		}
		if unicode.IsDigit(ch) {
			check += int(ch-'0') * coeff
			coeff--
		} else if ch == 'X' && coeff == 1 {
			check += 10
			coeff--
		} else if ch != '-' {
			return false
		}
	}
	if coeff != 0 {
		return false
	}
	if check%11 != 0 {
		return false
	}
	return true
}
