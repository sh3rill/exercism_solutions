package tree

import (
	"errors"
	"fmt"
)

type Record struct {
	ID, Parent int
}

func (r Record) String() string {
	s := fmt.Sprintf("{Id:%d, Parent:%d}", r.ID, r.Parent)
	return s
}

type Node struct {
	ID       int
	Children []*Node
}

type Mismatch struct{}

func (m Mismatch) Error() string {
	return "c"
}

func (r Record) isValid(max int) bool {
	if r.ID >= 0 && r.ID < max &&
		r.Parent >= 0 && r.Parent <= r.ID {
		return true
	}
	return false
}

func AddChild(root, node *Node, size int) {
	if root.Children == nil {
		root.Children = make([]*Node, 0, size)
	}
	i := len(root.Children)
	root.Children = append(root.Children, nil)
	for ; i >= 1; i-- {
		if root.Children[i-1].ID > node.ID {
			root.Children[i] = root.Children[i-1]
		} else {
			break
		}
	}
	root.Children[i] = node
}

func Build(records []Record) (*Node, error) {
	l := len(records)
	if l == 0 {
		return nil, nil
	}
	var root *Node
	//sort.Sort(Records(records))
	nodes := make([]Node, l)
	for i := range nodes {
		nodes[i].ID = i
	}
	for _, record := range records[:] {
		if record.isValid(l) {
			if record.Parent != record.ID {
				AddChild(&nodes[record.Parent], &nodes[record.ID], l)
			} else if record.ID == 0 {
				root = &nodes[0]
			} else {
				return nil, errors.New("Only root can have same parent")
			}
		} else {
			return nil, errors.New("invalid id")
		}
	}
	return root, nil
}

func chk(n *Node, m int) (err error) {
	if n.ID > m {
		return fmt.Errorf("z")
	} else if n.ID == m {
		return fmt.Errorf("y")
	} else {
		for i := 0; i < len(n.Children); i++ {
			err = chk(n.Children[i], m)
			if err != nil {
				return
			}
		}
		return
	}
}
