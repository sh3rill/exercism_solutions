package phonenumber

import (
	"errors"
	"fmt"
)

// Number convert input string to phone number
func Number(in string) (string, error) {
	out := make([]rune, 10)
	i := 0
	international := false
	for _, ch := range in {
		if ch >= '0' && ch <= '9' {
			if i == 0 && ch == '1' {
				international = true
				continue
			}
			if i < 10 {
				out[i] = ch
				i++
			} else if international {
				international = false
			} else {
				return "", errors.New("more digits than needed")
			}
		}
	}
	if i == 10 && out[0] > '1' && out[3] > '1' {
		return string(out), nil
	}
	return "", errors.New("wrong format")
}

// Format format an input string in to phone number format
func Format(in string) (string, error) {
	out, err := Number(in)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("(%s) %s-%s", out[0:3], out[3:6], out[6:10]), nil
}

// AreaCode get the area code from a phone number
func AreaCode(in string) (string, error) {
	out, err := Number(in)
	if err != nil {
		return "", err
	}
	return out[0:3], nil
}
