package matrix

import (
	"errors"
	"strconv"
	"strings"
)

type Matrix struct {
	row, col int
	elem     []int
}

func New(s string) (*Matrix, error) {
	lines := strings.Split(s, "\n")
	k := 0
	m := Matrix{0, 0, nil}
	for i := 0; i < len(lines); i++ {
		parts := strings.Split(strings.TrimSpace(lines[i]), " ")
		if i == 0 {
			m.row = len(lines)
			m.col = len(parts)
			m.elem = make([]int, m.row*m.col)
		} else if m.col != len(parts) {
			return nil, errors.New("Matrix col lenght mismatch")
		}
		for j := 0; j < len(parts); j++ {
			n, err := strconv.Atoi(parts[j])
			if err != nil {
				return nil, err
			}
			m.elem[k] = n
			k++
		}
	}

	return &m, nil
}

func (m *Matrix) Rows() [][]int {
	var rows = make([][]int, m.row)
	k := 0
	for i := 0; i < m.row; i++ {
		rows[i] = make([]int, m.col)
		for j := 0; j < m.col; j++ {
			rows[i][j] = m.elem[k]
			k++
		}
	}
	return rows
}

func (m *Matrix) Cols() [][]int {
	var cols = make([][]int, m.col)
	for i := 0; i < m.col; i++ {
		cols[i] = make([]int, m.row)
		for j := 0; j < m.row; j++ {
			cols[i][j] = m.elem[j*m.col+i]
		}

	}
	return cols
}
func (m *Matrix) Set(r, c, val int) bool {
	if r >= 0 && r < m.row && c >= 0 && c < m.col {
		m.elem[r*m.col+c] = val
		return true
	}
	return false
}
