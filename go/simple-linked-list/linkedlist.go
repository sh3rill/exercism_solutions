package linkedlist

import (
	"errors"
	"fmt"
)

// Element element of list
type Element struct {
	data int
	next *Element
}

// List linked list
type List struct {
	head *Element
	size int
}

// New get a new linked list
func New(args []int) *List {
	l := new(List)
	var last *Element
	for _, n := range args {
		e := &Element{data: n}
		if last != nil {
			last.next = e
		} else {
			l.head = e
		}
		last = e
		l.size++
	}
	return l
}

func (e *Element) String() string {
	return fmt.Sprintf("%d->[%v]", e.data, e.next)
}
func (l *List) String() string {
	return fmt.Sprintf("{%d->[%v]}", l.size, l.head)
}

// Size get size of list
func (l *List) Size() int {
	return l.size
}

// Push push int the front
func (l *List) Push(n int) {
	e := &Element{data: n, next: nil}
	var last *Element
	if l.head != nil {
		for last = l.head; last.next != nil; last = last.next {
		}
		last.next = e
	} else {
		l.head = e
	}
	l.size++
}

// Pop pop one element from front of list
func (l *List) Pop() (int, error) {
	if l.head == nil {
		return -1, errors.New("Empty")
	}
	e := l.head
	var last *Element
	for ; e.next != nil; e = e.next {
		last = e
	}
	if last != nil {
		last.next = nil
	} else {
		l.head = nil
	}
	e.next = nil
	l.size--
	return e.data, nil
}

// Array convert to array
func (l *List) Array() []int {
	arr := make([]int, l.size)
	for i, e := 0, l.head; e != nil; i, e = i+1, e.next {
		arr[i] = e.data
	}
	return arr
}

// Reverse reverse a node and return new root
func (e *Element) Reverse() *Element {
	if e.next == nil {
		return e
	}
	next := e.next
	root := next.Reverse()
	next.next = e
	e.next = nil
	return root
}

// Clone clone a list
func (l *List) Clone() *List {
	l1 := new(List)
	var last *Element
	for e := l.head; e != nil; e = e.next {
		e1 := &Element{data: e.data}
		if last == nil {
			l1.head = e1
		} else {
			last.next = e1
		}
		last = e1
		l1.size++
	}
	return l1
}

// Reverse reverse a linked list
func (l *List) Reverse() *List {
	l1 := l.Clone()
	if l1.head != nil {
		l1.head = l1.head.Reverse()
	}
	return l1
}
