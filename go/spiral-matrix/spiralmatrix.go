package spiralmatrix

import "fmt"

type Direction int
type Orientation int

const (
	None Direction = iota
	Up
	Down
	Left
	Right
)

const (
	Clockwise Orientation = iota
	CounterOrientation
)

type Marker struct {
	x, y   int
	matrix [][]int
}

var directionStr = []string{
	"None",
	"Up",
	"Down",
	"Left",
	"Right",
}

func (d Direction) String() string {
	return directionStr[d]
}

func (m *Marker) String() string {
	return fmt.Sprintf("(%d,%d)=%d", m.x, m.y, m.Current())
}

func (d Direction) Next(o Orientation) Direction {
	switch d {
	case Up:
		if o == Clockwise {
			return Right
		}
		return Left
	case Down:
		if o == Clockwise {
			return Left
		}
		return Right
	case Left:
		if o == Clockwise {
			return Up
		}
		return Down
	case Right:
		if o == Clockwise {
			return Down
		}
		return Up
	}
	return None
}

func NewMarker(m [][]int) *Marker {
	for i := 0; i < len(m); i++ {
		for j := 0; j < len(m[i]); j++ {
			m[i][j] = 0
		}
	}
	return &Marker{0, 0, m}
}

func (m *Marker) Set(n int) {
	m.matrix[m.y][m.x] = n
}

func (m *Marker) Current() int {
	return m.matrix[m.y][m.x]
}
func (m *Marker) IsEmpty() bool {
	return m.Isvalid() && m.matrix[m.y][m.x] == 0
}

func (m *Marker) Isvalid() bool {
	if m.y >= len(m.matrix) || m.y < 0 || m.x < 0 || m.x >= len(m.matrix[m.y]) {
		return false
	}
	return true
}

func (m *Marker) Move(dir Direction) bool {
	x, y := m.x, m.y
	switch dir {
	case Up:
		m.y--
	case Down:
		m.y++
	case Left:
		m.x--
	case Right:
		m.x++
	}
	if !m.IsEmpty() {
		m.x, m.y = x, y
		return false
	}
	return true
}

func SpiralMatrix(n int) [][]int {
	matrix := make([][]int, n)
	for i := 0; i < n; i++ {
		matrix[i] = make([]int, n)
	}
	m := NewMarker(matrix)
	orientation := Clockwise
	var direction Direction
	if orientation == Clockwise {
		direction = Right
	} else {
		direction = Down
	}
	limit := n * n
	for i := 1; i <= limit; i++ {
		m.Set(i)
		if i < limit {
			for !m.Move(direction) {
				direction = direction.Next(orientation)
			}
		}
	}
	return matrix
}
