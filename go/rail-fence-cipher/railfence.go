package railfence

import (
	"unicode"
)

// filter filter space and special chars
func filter(input string) []rune {
	r := make([]rune, 0, len(input))
	for _, ch := range input {
		if !unicode.IsSpace(ch) {
			r = append(r, ch)
		}
	}
	return r
}

// Fence get fence data
func Fence(input []rune, count int) [][]rune {
	rail := make([][]rune, count)
	size := len(input)
	for j := 0; j < size; {
		for i := 0; i < count && j < size; i++ {
			rail[i] = append(rail[i], input[j])
			j++
		}
		for i := count - 2; i > 0 && j < size; i-- {
			rail[i] = append(rail[i], input[j])
			j++
		}
	}
	return rail
}

// Rails get rails from e
func Rails(input []rune, count int) [][]rune {
	size := len(input)
	sizes := make([]int, count)
	arr := make([][]rune, count)
	for j := 0; j < size; {
		for i := 0; i < count && j < size; i++ {
			sizes[i]++
			j++
		}
		for i := count - 2; i > 0 && j < size; i-- {
			sizes[i]++
			j++
		}
	}
	total, start := 0, 0
	for i := 0; i < count; i++ {
		total += sizes[i]
		arr[i] = input[start:total]
		start = total
	}
	return arr
}

// Encode encode string
func Encode(input string, rails int) string {
	plain := filter(input)
	cipher := make([]rune, 0, len(plain))
	fence := Fence(plain, rails)
	for i := 0; i < len(fence); i++ {
		for j := 0; j < len(fence[i]); j++ {
			ch := fence[i][j]
			cipher = append(cipher, ch)
		}
	}

	return string(cipher)
}

// Decode decode string
func Decode(input string, rails int) string {
	cipher := []rune(input)
	plain := make([]rune, 0, len(cipher))
	index := make([]int, rails)
	arr := Rails(cipher, rails)
	update := true
	for update {
		update = false
		for i := 0; i < rails; i++ {
			j := index[i]
			if j < len(arr[i]) {
				plain = append(plain, arr[i][j])
				index[i]++
				update = true
			}
		}
		for i := rails - 2; i > 0; i-- {
			j := index[i]
			if j < len(arr[i]) {
				plain = append(plain, arr[i][j])
				index[i]++
				update = true
			}
		}
	}

	return string(plain)
}
