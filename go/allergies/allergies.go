package allergies

var allergyList = []string{
	"eggs",
	"peanuts",
	"shellfish",
	"strawberries",
	"tomatoes",
	"chocolate",
	"pollen",
	"cats",
}
var allergymap = map[string]uint{
	"eggs":         1,
	"peanuts":      2,
	"shellfish":    4,
	"strawberries": 8,
	"tomatoes":     16,
	"chocolate":    32,
	"pollen":       64,
	"cats":         128,
}

// Allergies get allergies with all the substance
func Allergies(score uint) []string {
	list := make([]string, 0, len(allergyList))
	var i uint
	var l = uint(len(allergyList))
	for i = 0; i < l; i++ {
		if 1<<i&score != 0 {
			list = append(list, allergyList[i])
		}
	}
	return list
}

// AllergicTo check if score indicate allergy with the substance
func AllergicTo(score uint, substance string) bool {
	x, ok := allergymap[substance]
	return ok && x&score != 0
}
