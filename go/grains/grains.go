package grains

import (
	"errors"
	"math"
)

func Square(n int) (uint64, error) {
	if n <= 0 || n > 64 {
		return 0, errors.New("n should be a positive interger less than 65")
	}
	return 1 << uint64(n-1), nil
}

func Total() uint64 {
	return math.MaxUint64
}
